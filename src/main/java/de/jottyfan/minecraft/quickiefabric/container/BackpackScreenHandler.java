package de.jottyfan.minecraft.quickiefabric.container;

import de.jottyfan.minecraft.quickiefabric.init.RegistryManager;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.util.Hand;

public class BackpackScreenHandler extends ScreenHandler {

	private final BackpackInventory backpackInventory;
	private final PlayerEntity player;
	private final ItemStack thisStack;
	private final Hand hand;

	public BackpackScreenHandler(int syncId, PlayerInventory playerInventory, PacketByteBuf buf) {
		super(RegistryManager.BACKPACK_SCREEN_HANDLER, syncId);
		this.player = playerInventory.player;
		thisStack = buf.readItemStack();
		this.hand = buf.readByte() > 0 ? Hand.MAIN_HAND : Hand.OFF_HAND;
		this.backpackInventory = BackpackInventory.getInventory(this, player, thisStack);

		backpackInventory.onOpen(player);
		for (int y = 0; y < 6; y++) {
			for (int x = 0; x < 9; ++x) {
				this.addSlot(new Slot(backpackInventory, x + y * 9, 8 + x * 18, -10 + y * 18));
			}
		}
		for (int y = 0; y < 3; ++y) {
			for (int x = 0; x < 9; ++x) {
				this.addSlot(new Slot(playerInventory, x + (y * 9) + 9, 8 + x * 18, 112 + y * 18));
			}
		}
		for (int x = 0; x < 9; ++x) {
			this.addSlot(new Slot(playerInventory, x, 8 + x * 18, 170));
		}
	}

	public PlayerEntity getPlayer() {
		return this.player;
	}

	public Inventory getInventory() {
		return backpackInventory;
	}

	@Override
	public boolean canUse(PlayerEntity playerEntity) {
		return backpackInventory.canPlayerUse(playerEntity);
	}

	@Override
	public void onClosed(PlayerEntity player) {
		this.backpackInventory.setHand(hand);
		this.backpackInventory.onClose(player);
		super.onClosed(player);
	}

	@Override
	public ItemStack quickMove(PlayerEntity player, int index) {
		ItemStack itemStack = ItemStack.EMPTY;
		Slot slot = (Slot) this.slots.get(index);
		if (slot != null && slot.hasStack()) {
			ItemStack itemStack2 = slot.getStack();
			itemStack = itemStack2.copy();
			if (index < 54 ? !this.insertItem(itemStack2, 54, this.slots.size(), true)
					: !this.insertItem(itemStack2, 0, 54, false)) {
				return ItemStack.EMPTY;
			}
			if (itemStack2.isEmpty()) {
				slot.setStack(ItemStack.EMPTY);
			} else {
				slot.markDirty();
			}
		}
		return itemStack;
	}

	@Override
	public void onSlotClick(int slotId, int quickCraftData, SlotActionType actionType, PlayerEntity playerEntity) {
		if (SlotActionType.QUICK_CRAFT.equals(actionType)) {
			return;
		}
		Slot slot = slotId >= 0 && slotId < this.slots.size() ? this.slots.get(slotId) : null;
		ItemStack stack = slot == null ? ItemStack.EMPTY : slot.getStack();
		if (!stack.getName().equals(thisStack.getName())) {
			if (SlotActionType.THROW.equals(actionType)) {
				super.onSlotClick(slotId, quickCraftData, SlotActionType.PICKUP, playerEntity);
			} else {
				super.onSlotClick(slotId, quickCraftData, actionType, playerEntity);
			}
		}
	}

	@Environment(EnvType.CLIENT)
	public int getRows() {
		return 6;
	}
}
