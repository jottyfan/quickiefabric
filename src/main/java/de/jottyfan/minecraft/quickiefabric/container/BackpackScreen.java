package de.jottyfan.minecraft.quickiefabric.container;

import de.jottyfan.minecraft.quickiefabric.init.RegistryManager;
import de.jottyfan.minecraft.quickiefabric.items.ItemBackpack;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.gui.screen.ingame.ScreenHandlerProvider;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

/**
 *
 * @author jotty
 *
 */
@Environment(EnvType.CLIENT)
public class BackpackScreen extends HandledScreen<BackpackScreenHandler>
		implements ScreenHandlerProvider<BackpackScreenHandler> {
	private final static Identifier TEXTURE = new Identifier(RegistryManager.QUICKIEFABRIC, "textures/gui/backpack.png");
	private final static Identifier SLOT_TEXTURE = new Identifier(RegistryManager.QUICKIEFABRIC, "textures/gui/slot.png");
	private final Integer containerHeight = 222;
	private final Integer containerWidth = 176;

	public BackpackScreen(BackpackScreenHandler handler, PlayerInventory inventory, Text text) {
		super(handler, inventory, Text.empty());
	}

	@Override
	protected void init() {
		super.init();
		this.playerInventoryTitleY = -1000;
	}

	private void drawSlots(DrawContext context, int guiX, int guiY) {
		context.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
		for (int y = 0; y < (ItemBackpack.SLOTSIZE / 9); y++) {
			for (int x = 0; x < 9; x++) {
				context.drawTexture(SLOT_TEXTURE, guiX + 7 + (x * 18), guiY + 17 + (y * 18), 0, 0, 18, 18);
			}
		}
	}

	@Override
	public void render(DrawContext drawContext, int mouseX, int mouseY, float partialTicks) {
		this.renderInGameBackground(drawContext);
		super.render(drawContext, mouseX, mouseY, partialTicks);
		this.drawMouseoverTooltip(drawContext, mouseX, mouseY);
	}

	@Override
	protected void drawBackground(DrawContext context, float delta, int mouseX, int mouseY) {
		context.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
		int guiX = (this.width - this.containerWidth) / 2;
		int guiY = (this.height - this.containerHeight) / 2;
		context.drawTexture(TEXTURE, guiX, guiY, 0, 0, containerWidth, containerHeight);
		drawSlots(context, guiX, guiY);
	}
}
