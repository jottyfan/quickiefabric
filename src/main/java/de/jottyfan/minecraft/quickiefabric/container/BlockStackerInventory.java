package de.jottyfan.minecraft.quickiefabric.container;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.jottyfan.minecraft.quickiefabric.items.ItemBackpack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.nbt.NbtList;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Hand;

/**
 *
 * @author jotty
 *
 */
public class BlockStackerInventory extends SimpleInventory {
	private static final String NBT_BLOCKSTACKER = "blockstacer";
	private static final String NBT_SLOT = "slot";
	private static final String NBT_ITEMS = "items";

	private Hand hand;

	private BlockStackerInventory(NbtCompound tag, BlockStackerScreenHandler handler) {
		super(1);
		readItemsFromTag(super.size(), tag);
	}

	public BlockStackerInventory(ItemStack stack) {
		this(init(stack).getOrCreateNbt().getCompound(NBT_BLOCKSTACKER), null);
	}

	public static final BlockStackerInventory getInventory(BlockStackerScreenHandler handler, PlayerEntity player,
			ItemStack stack) {
		return new BlockStackerInventory(init(stack).getOrCreateNbt().getCompound(NBT_BLOCKSTACKER), handler);
	}

	private final static ItemStack init(ItemStack stack) {
		if (stack != null) {
			if (!stack.getOrCreateNbt().contains(NBT_BLOCKSTACKER)) {
				stack.getOrCreateNbt().put(NBT_BLOCKSTACKER, new NbtCompound());
			}
		}
		return stack;
	}

	@Override
	public void onOpen(PlayerEntity player) {
		super.onOpen(player);
		player.playSound(SoundEvents.BLOCK_CHEST_OPEN, SoundCategory.PLAYERS, 1f, 1f);
	}

	@Override
	public void onClose(PlayerEntity player) {
		super.onClose(player);
		ItemStack stack = player.getStackInHand(hand);
		if (stack != null) {
			stack.getOrCreateNbt().put(NBT_BLOCKSTACKER, writeItemsToTag(super.size()));
		}
		player.getStackInHand(hand).setNbt(stack.getNbt());
		player.playSound(SoundEvents.BLOCK_CHEST_CLOSE, SoundCategory.PLAYERS, 1f, 1f);
	}

	private void readItemsFromTag(Integer size, NbtCompound tag) {
		NbtList listTag = tag.getList(NBT_ITEMS, NbtElement.COMPOUND_TYPE);
		for (int i = 0; i < listTag.size(); ++i) {
			NbtCompound compoundTag = listTag.getCompound(i);
			int slot = compoundTag.getInt(NBT_SLOT);
			if (slot >= 0 && slot < size) {
				super.setStack(slot, ItemStack.fromNbt(compoundTag));
			}
		}
	}

	private NbtCompound writeItemsToTag(int slotsize) {
		NbtList listTag = new NbtList();
		for (int slot = 0; slot < slotsize; ++slot) {
			ItemStack itemStack = super.getStack(slot);
			if (!(itemStack == null) && !itemStack.isEmpty()) {
				listTag.add(prepareCompoundTag(slot, itemStack));
			}
		}
		NbtCompound tag = new NbtCompound();
		tag.put(NBT_ITEMS, listTag);
		return tag;
	}

	private static final NbtCompound prepareCompoundTag(Integer slot, ItemStack stack) {
		NbtCompound compoundTag = new NbtCompound();
		compoundTag.putInt(NBT_SLOT, slot);
		stack.writeNbt(compoundTag);
		return compoundTag;
	}

	public void setHand(Hand hand) {
		this.hand = hand;
	}

	/**
	 * get the items from the itemStack that contains the backpack
	 *
	 * @param itemStack the itemStack of the backpack
	 * @return the list of found itemStacks in the backpack
	 */
	public static List<ItemStack> getItemsFromBackpack(ItemStack itemStack) {
		NbtCompound backpackNbt = init(itemStack).getOrCreateNbt().getCompound(NBT_BLOCKSTACKER);
		NbtList listTag = backpackNbt.getList(NBT_ITEMS, NbtElement.COMPOUND_TYPE);
		List<ItemStack> items = new ArrayList<>();
		for (int i = 0; i < listTag.size(); ++i) {
			NbtCompound compoundTag = listTag.getCompound(i);
			int slot = compoundTag.getInt(NBT_SLOT);
			if (slot >= 0 && slot < ItemBackpack.SLOTSIZE) {
				ItemStack stack = ItemStack.fromNbt(compoundTag);
				items.add(stack);
			}
		}
		return items;
	}

	/**
	 * set the items in the itemStack that contains the backpack
	 *
	 * @param itemStack  the backpack's itemStack
	 * @param itemStacks the collection of lists of itemStacks for the backpack
	 */
	public static void setItemsToBackpack(ItemStack itemStack, Collection<List<ItemStack>> itemStacks) {
		NbtList listTag = new NbtList();
		Integer slot = 0;
		for (List<ItemStack> stacks : itemStacks) {
			if (stacks != null && stacks.size() > 0) {
				ItemStack stack = stacks.get(0);
				Integer leftCount = 0;
				for (ItemStack is : stacks) {
					leftCount += is.getCount();
				}
				while (leftCount > 0) {
					if (leftCount > stack.getMaxCount()) {
						stack.setCount(stack.getMaxCount());
						leftCount = leftCount - stack.getMaxCount();
					} else {
						stack.setCount(leftCount);
						leftCount = 0;
					}
					listTag.add(prepareCompoundTag(slot, stack));
					slot++;
				}
			}
		}
		NbtCompound tag = new NbtCompound();
		tag.put(NBT_ITEMS, listTag);
		itemStack.getOrCreateNbt().put(NBT_BLOCKSTACKER, tag);
	}

	/**
	 * replace every slot of the backpack with the content of backpackInventory
	 *
	 * @param itemStack the backpack's itemStack
	 * @param backpackInventory the replacement inventory
	 */
	public static void setItemsToBackpack(ItemStack itemStack, BlockStackerInventory backpackInventory) {
		Collection<List<ItemStack>> itemStacks = new ArrayList<>();
		for (int i = 0; i < backpackInventory.size(); i++) {
			ItemStack stack = backpackInventory.getStack(i);
			List<ItemStack> list = new ArrayList<>();
			list.add(stack);
			itemStacks.add(list);
		}
		setItemsToBackpack(itemStack, itemStacks);
	}
}
