package de.jottyfan.minecraft.quickiefabric.blockentity;

import net.minecraft.block.entity.BlockEntityType;

/**
 *
 * @author jotty
 *
 */
public class QuickieFabricBlockEntity {
	public static BlockEntityType<BlockSpreaderEntity> BLOCKSPREADER_ENTITY;
	public static BlockEntityType<BlockStackerEntity> BLOCKSTACKER_ENTITY;
	public static BlockEntityType<ItemHoarderBlockEntity> ITEMHOARDER;
	public static BlockEntityType<MonsterHoarderBlockEntity> MONSTERHOARDER;
	public static BlockEntityType<EmptyLavaHoarderBlockEntity> EMPTYLAVAHOARDER;
	public static BlockEntityType<DrillBlockDownEntity> DRILL_DOWN;
	public static BlockEntityType<DrillBlockEastEntity> DRILL_EAST;
	public static BlockEntityType<DrillBlockSouthEntity> DRILL_SOUTH;
	public static BlockEntityType<DrillBlockWestEntity> DRILL_WEST;
	public static BlockEntityType<DrillBlockNorthEntity> DRILL_NORTH;
}
