package de.jottyfan.minecraft.quickiefabric.blockentity;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class DrillBlockDownEntity extends DrillBlockEntity {

	private static final Integer MAXDRILLSTEP = 20;

	public DrillBlockDownEntity(BlockPos pos, BlockState state) {
		super(QuickieFabricBlockEntity.DRILL_DOWN, pos, state, MAXDRILLSTEP);
	}

	public static void tick(World world, BlockPos pos, BlockState state, BlockEntity be) {
		if (be instanceof DrillBlockDownEntity) {
			DrillBlockDownEntity dbe = (DrillBlockDownEntity) be;
			DrillBlockEntity.tick(world, pos, state, dbe, MAXDRILLSTEP, generateBlockPos(pos));
		}
	}

	public static final List<BlockPos> generateBlockPos(BlockPos pos) {
		List<BlockPos> list = new ArrayList<>();
		Integer tracesMod = pos.getY() % 8;
		tracesMod = tracesMod < 0 ? tracesMod * -1 : tracesMod; // lower that 0 makes it negative
		if (tracesMod != 0) {
			list.add(pos.north());
		}
		if (tracesMod != 1) {
			list.add(pos.north().west());
		}
		if (tracesMod != 2) {
			list.add(pos.west());
		}
		if (tracesMod != 3) {
			list.add(pos.south().west());
		}
		if (tracesMod != 4) {
			list.add(pos.south());
		}
		if (tracesMod != 5) {
			list.add(pos.south().east());
		}
		if (tracesMod != 6) {
			list.add(pos.east());
		}
		if (tracesMod != 7) {
			list.add(pos.north().east());
		}
		list.add(pos.down()); // must be last position
		return list;
	}
}
