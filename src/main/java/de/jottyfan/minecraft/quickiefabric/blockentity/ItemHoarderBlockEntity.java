package de.jottyfan.minecraft.quickiefabric.blockentity;

import java.util.ArrayList;
import java.util.List;

import de.jottyfan.minecraft.quickiefabric.text.PrefixedText;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.Entity.RemovalReason;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class ItemHoarderBlockEntity extends LootableContainerBlockEntity {
	private DefaultedList<ItemStack> stacks;
	private float suckradius;

	public ItemHoarderBlockEntity(BlockPos pos, BlockState state) {
		super(QuickieFabricBlockEntity.ITEMHOARDER, pos, state);
		stacks = DefaultedList.ofSize(54, ItemStack.EMPTY);
		setSuckradius(4f); // TODO: make it level up - able and start with 2
	}

	// TODO: see https://fabricmc.net/wiki/tutorial:containers for a real chest

	public final static boolean setStackToSlots(ItemStack stack, List<ItemStack> stacks) {
		for (ItemStack slot : stacks) {
			if (slot.getItem().equals(stack.getItem())) {
				slot.increment(stack.getCount());
				return true;
			}
		} // if not found, seek for an empty stack instead
		for (ItemStack slot : stacks) {
			if (slot.isEmpty()) {
				Integer index = stacks.indexOf(slot);
				stacks.set(index, stack.copy());
				return true;
			}
		}
		return false;
	}

	public static void tick(World world, BlockPos pos, BlockState state, BlockEntity be) {
		if (be instanceof ItemHoarderBlockEntity) {
			ItemHoarderBlockEntity ihbe = (ItemHoarderBlockEntity) be;
			Box box = new Box(pos).expand(ihbe.getSuckradius());
			List<Entity> entities = world.getOtherEntities(null, box);
			for (Entity entity : entities) {
				if (entity instanceof ItemEntity) {
					ItemEntity itemEntity = (ItemEntity) entity;
					if (itemEntity.isAlive()) {
						ItemStack stack = itemEntity.getStack();
						if (stack != null) {
							if (ItemHoarderBlockEntity.setStackToSlots(stack, ihbe.getStacks())) {
								itemEntity.remove(RemovalReason.DISCARDED);
							} // else inventory is full
						}
					}
				}
			}
		}
	}

	@Override
	public void writeNbt(NbtCompound nbt) {
		super.writeNbt(nbt);
		if (!this.serializeLootTable(nbt)) {
			Inventories.writeNbt(nbt, this.stacks);
		}
	}

	@Override
	public void readNbt(NbtCompound nbt) {
		super.readNbt(nbt);
		this.stacks = DefaultedList.ofSize(this.size(), ItemStack.EMPTY);
		if (!this.deserializeLootTable(nbt)) {
			Inventories.readNbt(nbt, this.stacks);
		}
	}

	public List<ItemStack> getStacks() {
		return stacks;
	}

	@Override
	public int size() {
		return 54; // container chest size (9 * 6)
	}

	@Override
	protected DefaultedList<ItemStack> getInvStackList() {
		return stacks;
	}

	@Override
	protected void setInvStackList(DefaultedList<ItemStack> list) {
		this.stacks = list;
	}

	@Override
	protected ScreenHandler createScreenHandler(int syncId, PlayerInventory playerInventory) {
		// TODO: implement, see https://fabricmc.net/wiki/tutorial:containers
		return null;
	}

	@Override
	protected Text getContainerName() {
		return Text.translatable("container.itemhoarder");
	}

	/**
	 * get a summary of content for the chat box
	 *
	 * @return the summary
	 */
	public List<Text> getSummary() {
		List<Text> list = new ArrayList<>();
		for (ItemStack stack : stacks) {
			Item item = stack.getItem();
			if (item != Items.AIR) {
				Text text = PrefixedText.instance(String.format("%dx ", stack.getCount()).concat("%s"),
						Text.translatable(stack.getTranslationKey()));
				list.add(text);
			}
		}
		return list;
	}

	public float getSuckradius() {
		return suckradius;
	}

	public void setSuckradius(float suckradius) {
		this.suckradius = suckradius;
	}
}
