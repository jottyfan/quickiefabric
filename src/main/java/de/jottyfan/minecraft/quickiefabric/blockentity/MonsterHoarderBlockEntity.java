package de.jottyfan.minecraft.quickiefabric.blockentity;

import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.PiglinEntity;
import net.minecraft.entity.mob.WardenEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class MonsterHoarderBlockEntity extends BlockEntity {

	private float suckradius;

	public MonsterHoarderBlockEntity(BlockPos pos, BlockState state) {
		super(QuickieFabricBlockEntity.MONSTERHOARDER, pos, state);
		setSuckradius(8f); // TODO: make it level up - able and start with 2
	}

	public static void tick(World world, BlockPos pos, BlockState state, BlockEntity be) {
		if (be instanceof MonsterHoarderBlockEntity) {
			MonsterHoarderBlockEntity mhbe = (MonsterHoarderBlockEntity) be;
			Box box = new Box(pos).expand(mhbe.getSuckradius());
			List<Entity> entities = world.getOtherEntities(null, box);
			for (Entity entity : entities) {
				if (entity instanceof WardenEntity) {
					entity.kill();
				} else if (entity instanceof PiglinEntity) {
					entity.kill();
				} else if (entity instanceof HostileEntity) {
					entity.setOnFireFor(90);
				}
			}
		}
	}

	public float getSuckradius() {
		return suckradius;
	}

	public void setSuckradius(float suckradius) {
		this.suckradius = suckradius;
	}
}
