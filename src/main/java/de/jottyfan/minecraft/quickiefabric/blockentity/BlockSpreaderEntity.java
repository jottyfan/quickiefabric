package de.jottyfan.minecraft.quickiefabric.blockentity;

import java.util.Iterator;

import de.jottyfan.minecraft.quickiefabric.container.ImplementedInventory;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class BlockSpreaderEntity extends BlockEntity implements ImplementedInventory {
	private final DefaultedList<ItemStack> inventory = DefaultedList.ofSize(9, ItemStack.EMPTY);

	public BlockSpreaderEntity(BlockPos blockPos, BlockState blockState) {
		super(QuickieFabricBlockEntity.BLOCKSPREADER_ENTITY, blockPos, blockState);
	}

	public static void tick(World world, BlockPos pos, BlockState state, BlockSpreaderEntity entity) {
		if (!world.isClient) {
			ItemStack stack = entity.plopNextItemStack();
			if (stack != null) {
				ItemScatterer.spawn(world, pos.getX(), pos.getY(), pos.getZ(), stack);
			}
		}
	}

	/**
	 * find the next itemstack
	 *
	 * @return the next item stack
	 */
	public ItemStack plopNextItemStack() {
		Iterator<ItemStack> i = inventory.iterator();
		Integer index = 0;
		while (i.hasNext()) {
			ItemStack stack = i.next();
			if (stack != null && stack.getItem() != null && stack.getCount() > 0) {
				inventory.get(index).decrement(8);
				if (stack.getCount() > 8) {
					stack.setCount(8);
				}
				return stack;
			}
			index++;
		}
		return null;
	}

	@Override
	public DefaultedList<ItemStack> getItems() {
		return inventory;
	}

	@Override
	public int size() {
		return inventory.size();
	}
}
