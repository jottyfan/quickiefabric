package de.jottyfan.minecraft.quickiefabric.blockentity;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.jottyfan.minecraft.quickiefabric.blocks.help.BlockStacker;
import de.jottyfan.minecraft.quickiefabric.container.BlockStackerScreenHandler;
import de.jottyfan.minecraft.quickiefabric.container.ImplementedInventory;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class BlockStackerEntity extends BlockEntity implements NamedScreenHandlerFactory, ImplementedInventory {
	private static final Logger LOGGER = LogManager.getLogger(BlockStackerEntity.class);

	private final DefaultedList<ItemStack> inventory = DefaultedList.ofSize(BlockStackerScreenHandler.SLOTSIZE, ItemStack.EMPTY);

	public BlockStackerEntity(BlockPos blockPos, BlockState blockState) {
		super(QuickieFabricBlockEntity.BLOCKSTACKER_ENTITY, blockPos, blockState);
	}

	@Override
	public DefaultedList<ItemStack> getItems() {
		return inventory;
	}

	public List<ItemStack> getWhiteList() {
		int counter = 0;
		List<ItemStack> list = new ArrayList<>();
		for (ItemStack stack : inventory) {
			counter++;
			if (counter < 10) { // first 9 items are whitelist items
				list.add(stack);
			}
		}
		return list;
	}

	public List<ItemStack> getBlackList() {
		int counter = 0;
		List<ItemStack> list = new ArrayList<>();
		for (ItemStack stack : inventory) {
			counter++;
			if (counter > 9) { // second 9 items are blacklist items
				list.add(stack);
			}
		}
		return list;
	}

	@Override
  public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
      return new BlockStackerScreenHandler(syncId, playerInventory, this);
  }

  @Override
  public Text getDisplayName() {
      return Text.translatable(getCachedState().getBlock().getTranslationKey());
  }
	@Override
	public void readNbt(NbtCompound nbt) {
		super.readNbt(nbt);
		Inventories.readNbt(nbt, inventory);
	}

	@Override
	public void writeNbt(NbtCompound nbt) {
		Inventories.writeNbt(nbt, inventory);
		super.writeNbt(nbt);
	}

	/**
	 * if whitelist, return true if current == pattern; return false otherwise
	 *
	 * @param current   the current item stack
	 * @param pattern   the item stack to compare with
	 * @param whitelist if true, filter only current == pattern, if false, filter
	 *                  all but that
	 * @return true or false
	 */
	public static final Boolean filter(ItemStack current, ItemStack pattern, Boolean whitelist) {
		Boolean matches = pattern.getItem().equals(current.getItem());
		return whitelist ? matches : !matches;
	}

	public static void tick(World world, BlockPos pos, BlockState state, BlockStackerEntity entity) {
		if (!world.isClient) {
			pos.down();
			BlockStacker block = (BlockStacker) state.getBlock();
			BlockEntity source = world.getBlockEntity(pos.offset(block.getSourceOffset()));
			BlockEntity dest = world.getBlockEntity(pos.offset(block.getDestOffset()));
			Boolean sourceIsLootable = source instanceof LootableContainerBlockEntity;
			Boolean destIsLootable = dest instanceof LootableContainerBlockEntity;
			if (sourceIsLootable && destIsLootable) {
				LootableContainerBlockEntity lootableSource = (LootableContainerBlockEntity) source;
				LootableContainerBlockEntity lootableDest = (LootableContainerBlockEntity) dest;
				transferOneStack(lootableSource, lootableDest, entity.getWhiteList(), entity.getBlackList());
			}
		}
	}

	private static final Boolean transferOneStack(LootableContainerBlockEntity source, LootableContainerBlockEntity dest, List<ItemStack> whiteList, List<ItemStack> blackList) {

		// whitelist behaviour
		List<Item> checked = new ArrayList<>();

		// this way, we block whitelist items that are in the blacklist
		for (ItemStack stack : blackList) {
			if (stack != null && !stack.isEmpty()) {
				checked.add(stack.getItem());
			}
		}
		Boolean found = false;

		if (hasItems(whiteList)) {
			Item matchItem = findNextItem(whiteList, checked);
			while(!found && matchItem != null) {
				checked.add(matchItem);
				List<Item> matchItems = new ArrayList<>();
				matchItems.add(matchItem);
				found = transferOneStack(source, dest, matchItems, true);
				matchItem = findNextItem(whiteList, checked);
			}
		} else {
			// transport all but the items of the blacklist
			found = transferOneStack(source, dest, checked, false);
		}
		return found;
	}

	private static final Boolean transferOneStack(LootableContainerBlockEntity source, LootableContainerBlockEntity dest,
			List<Item> ignoreItems, Boolean whitelist) {
		Boolean result = false;
		Integer sourceSlot = findItemStackPos(source, ignoreItems, whitelist);
		if (sourceSlot != null && !Items.AIR.equals(source.getStack(sourceSlot).getItem())) {
			ItemStack sourceStack = source.getStack(sourceSlot);
			Integer destSlot = findItemStackPos(dest, sourceStack);
			if (destSlot != null) {
				Integer occupied = dest.getStack(destSlot).getCount();
				Integer free = dest.getStack(destSlot).getMaxCount() - occupied;
				Integer candidates = source.getStack(sourceSlot).getCount();
				Integer travellers = candidates > free ? free : candidates;
				if (travellers > 0) {
					LOGGER.debug("transfer {}/{} of {} from slot {} to slot {} on top of {} ones", travellers, candidates,
							source.getStack(sourceSlot).getItem().toString(), sourceSlot, destSlot, occupied);
					source.getStack(sourceSlot).decrement(travellers);
					if (source.getStack(sourceSlot).getCount() < 1) {
						source.removeStack(sourceSlot); // make empty slots really empty
					}
					dest.getStack(destSlot).increment(travellers);
					result = true;
				}
			} else {
				Integer destFreeSlot = findItemStackPos(dest, true);
				if (destFreeSlot != null) {
					LOGGER.debug("transfer all of {} from slot {} to slot {}", source.getStack(sourceSlot).getItem().toString(),
							sourceSlot, destSlot);
					dest.setStack(destFreeSlot, source.removeStack(sourceSlot));
					result = true;
				}
			}
		}
		return result;
	}

	private static final Boolean hasItems(List<ItemStack> list) {
		Boolean result = false;
		for (ItemStack stack : list) {
			result = result || (stack != null && !stack.isEmpty() && stack.getCount() > 0);
		}
		return result;
	}

	private static final Item findNextItem(List<ItemStack> inventory, List<Item> exclude) {
		for (ItemStack stack : inventory) {
			if (!stack.isEmpty()) {
				Item item = stack.getItem();
				if (!exclude.contains(item)) {
					return item;
				}
			}
		}
		return null;
	}

	private static final Integer findItemStackPos(LootableContainerBlockEntity lcbe, ItemStack sourceStack) {
		Integer counter = lcbe.size();
		while (counter > 0) {
			counter--;
			ItemStack stack = lcbe.getStack(counter);
			if (sourceStack.getItem().equals(stack.getItem())) {
				if (stack.getCount() < stack.getMaxCount()) {
					return counter;
				}
			}
		}
		return null;
	}

	private static final Integer findItemStackPos(LootableContainerBlockEntity lcbe, Boolean empty) {
		Integer counter = lcbe.size();
		while (counter > 0) {
			counter--;
			ItemStack stack = lcbe.getStack(counter);
			if (empty.equals(ItemStack.EMPTY.equals(stack))) {
				return counter;
			}
		}
		return null;
	}

	private static final Integer findItemStackPos(LootableContainerBlockEntity lcbe, List<Item> filterItems, Boolean whitelist) {
		if (whitelist == null) {
			whitelist = true;
			LOGGER.error("whitelist is null");
		}
		if (filterItems == null || filterItems.size() < 1) {
			if (whitelist) {
				return null;
			} else {
				return findItemStackPos(lcbe, false);
			}
		} else {
			Integer counter = lcbe.size();
			while (counter > 0) {
				counter--;
				ItemStack stack = lcbe.getStack(counter);
				Boolean found = whitelist ? filterItems.contains(stack.getItem()) : !filterItems.contains(stack.getItem());
				if (found) {
					return counter;
				}
			}
			return null;
		}
	}

	@Override
	public int size() {
		return inventory.size();
	}
}
