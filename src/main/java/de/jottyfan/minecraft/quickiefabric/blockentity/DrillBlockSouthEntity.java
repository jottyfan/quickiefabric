package de.jottyfan.minecraft.quickiefabric.blockentity;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class DrillBlockSouthEntity extends DrillBlockEntity {
	private static final Integer MAXDRILLSTEP = 24;

	public DrillBlockSouthEntity(BlockPos pos, BlockState state) {
		super(QuickieFabricBlockEntity.DRILL_SOUTH, pos, state, MAXDRILLSTEP);
	}

	public static void tick(World world, BlockPos pos, BlockState state, BlockEntity be) {
		if (be instanceof DrillBlockSouthEntity) {
			DrillBlockSouthEntity dbe = (DrillBlockSouthEntity) be;
			List<BlockPos> list = new ArrayList<>();
			list.add(pos.south());
			list.add(pos.south().up());
			list.add(pos.south().up().up());
			list.add(pos.south().up().up().up());
			list.add(pos.south().down()); // must be last position
			DrillBlockEntity.tick(world, pos, state, dbe, MAXDRILLSTEP, list);
		}
	}
}
