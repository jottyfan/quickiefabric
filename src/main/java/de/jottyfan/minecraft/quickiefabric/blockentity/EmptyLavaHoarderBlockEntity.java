package de.jottyfan.minecraft.quickiefabric.blockentity;

import java.util.Random;

import de.jottyfan.minecraft.quickiefabric.blocks.QuickieBlocks;
import de.jottyfan.minecraft.quickiefabric.items.QuickieItems;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class EmptyLavaHoarderBlockEntity extends BlockEntity {

	public EmptyLavaHoarderBlockEntity(BlockPos pos, BlockState state) {
		super(QuickieFabricBlockEntity.EMPTYLAVAHOARDER, pos, state);
	}

	public static final void spawnRandomItems(World world, BlockPos pos, Integer count) {
		world.spawnEntity(new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), new ItemStack(Items.DIAMOND, new Random().nextInt(count))));
		world.spawnEntity(new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(),	new ItemStack(Items.EMERALD, new Random().nextInt(count))));
		world.spawnEntity(new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(),	new ItemStack(Items.GOLD_ORE, new Random().nextInt(count))));
		world.spawnEntity(new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(),	new ItemStack(Items.IRON_ORE, new Random().nextInt(count))));
		world.spawnEntity(new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(),	new ItemStack(Items.COPPER_ORE, new Random().nextInt(count))));
		world.spawnEntity(new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(),	new ItemStack(Items.OBSIDIAN, new Random().nextInt(count))));
		world.spawnEntity(new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(),	new ItemStack(Items.LAPIS_LAZULI, new Random().nextInt(count))));
		world.spawnEntity(new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(),	new ItemStack(QuickieItems.SULPHOR, new Random().nextInt(count))));
	}

	/**
	 * sucks the lava that touches the block
	 *
	 * @param world the world
	 * @param pos   the pos
	 * @return true if lava was found
	 */
	private boolean suckLava(World world, BlockPos pos) {
		if (world == null) {
			return false;
		} else if (Blocks.LAVA.equals(world.getBlockState(pos).getBlock())) {
			world.setBlockState(pos, Blocks.AIR.getDefaultState());
			BlockPos up = pos.up();
			Random random = new Random();
			if (random.nextFloat() > 0.9f) {
				spawnRandomItems(world, up, 2);
			}
			return true;
		}
		return false;
	}

	public static void tick(World world, BlockPos pos, BlockState state, BlockEntity be) {
		if (be instanceof EmptyLavaHoarderBlockEntity) {
			EmptyLavaHoarderBlockEntity elhbe = (EmptyLavaHoarderBlockEntity) be;
			boolean found = elhbe.suckLava(world, pos.north());
			found = found || elhbe.suckLava(world, pos.south());
			found = found || elhbe.suckLava(world, pos.east());
			found = found || elhbe.suckLava(world, pos.west());
			found = found || elhbe.suckLava(world, pos.up());
			found = found || elhbe.suckLava(world, pos.down());
			if (found) {
				world.setBlockState(pos, QuickieBlocks.LAVAHOARDER.getDefaultState());
			}
		}
	}
}
