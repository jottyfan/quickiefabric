package de.jottyfan.minecraft.quickiefabric.blockentity;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class DrillBlockWestEntity extends DrillBlockEntity {

	private static final Integer MAXDRILLSTEP = 24;

	public DrillBlockWestEntity(BlockPos pos, BlockState state) {
		super(QuickieFabricBlockEntity.DRILL_WEST, pos, state, MAXDRILLSTEP);
	}

	public static void tick(World world, BlockPos pos, BlockState state, BlockEntity be) {
		if (be instanceof DrillBlockWestEntity) {
			DrillBlockWestEntity dbe = (DrillBlockWestEntity) be;
			List<BlockPos> list = new ArrayList<>();
			list.add(pos.west());
			list.add(pos.west().up());
			list.add(pos.west().up().up());
			list.add(pos.west().up().up().up());
			list.add(pos.west().down()); // must be last position
			DrillBlockEntity.tick(world, pos, state, dbe, MAXDRILLSTEP, list);
		}
	}
}
