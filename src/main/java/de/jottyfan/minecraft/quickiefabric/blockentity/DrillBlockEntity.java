package de.jottyfan.minecraft.quickiefabric.blockentity;

import java.util.List;

import de.jottyfan.minecraft.quickiefabric.blocks.QuickieBlocks;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public abstract class DrillBlockEntity extends BlockEntity {

	private Integer drillstep;
	private final Integer maxDrillStep;

	public DrillBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state, Integer maxDrillStep) {
		super(type, pos, state);
		this.maxDrillStep = maxDrillStep;
	}

	protected static final void moveBlockWithEntity(BlockPos from, BlockPos to, World world) {
		BlockEntity be = world.getBlockEntity(from);
		BlockState bs = world.getBlockState(from);
		if (be != null) {
			world.setBlockState(from, Blocks.AIR.getDefaultState());
			world.setBlockState(to, bs);
			world.addBlockEntity(be);
			world.removeBlockEntity(from);
		}
	}

	protected static final Boolean drill(BlockPos pos, List<BlockPos> toList, World world) {
		Boolean lastSuccess = false;
		for (BlockPos to : toList) {
			if (!world.getBlockState(to).isOf(Blocks.BEDROCK) && !world.getBlockState(to).isOf(QuickieBlocks.DRILLSTOP)) {
				world.breakBlock(to, true);
				lastSuccess = pos.down() != to; // no need for the falling one
			} else {
				lastSuccess = false; // in case that the last one is a bedrock or a drillstop
			}
		}
		return lastSuccess;
	}

	public static void tick(World world, BlockPos pos, BlockState state, DrillBlockEntity be, Integer maxDrillStep, List<BlockPos> drillPosition) {
		if (be.getDrillstep() < 1) {
			be.setDrillstep(maxDrillStep);
			if (drill(pos, drillPosition, world)) {
				moveBlockWithEntity(pos, drillPosition.get(drillPosition.size() - 1), world);
			}
		} else {
			be.doDrillstep();
		}
	}

	public void doDrillstep() {
		setDrillstep(getDrillstep() - 1);
	}

	public Integer getDrillstep() {
		return drillstep == null ? maxDrillStep : drillstep;
	}

	public void setDrillstep(Integer drillstep) {
		this.drillstep = drillstep;
	}
}
