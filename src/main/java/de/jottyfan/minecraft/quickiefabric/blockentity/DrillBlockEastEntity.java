package de.jottyfan.minecraft.quickiefabric.blockentity;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class DrillBlockEastEntity extends DrillBlockEntity {

	private static final Integer MAXDRILLSTEP = 24;

	public DrillBlockEastEntity(BlockPos pos, BlockState state) {
		super(QuickieFabricBlockEntity.DRILL_EAST, pos, state, MAXDRILLSTEP);
	}

	public static void tick(World world, BlockPos pos, BlockState state, BlockEntity be) {
		if (be instanceof DrillBlockEastEntity) {
			DrillBlockEastEntity dbe = (DrillBlockEastEntity) be;
			List<BlockPos> list = new ArrayList<>();
			list.add(pos.east());
			list.add(pos.east().up());
			list.add(pos.east().up().up());
			list.add(pos.east().up().up().up());
			list.add(pos.east().down()); // must be last position
			DrillBlockEntity.tick(world, pos, state, dbe, MAXDRILLSTEP, list);
		}
	}
}
