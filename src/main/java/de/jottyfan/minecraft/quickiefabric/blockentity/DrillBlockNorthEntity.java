package de.jottyfan.minecraft.quickiefabric.blockentity;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class DrillBlockNorthEntity extends DrillBlockEntity {

	private static final Integer MAXDRILLSTEP = 24;

	public DrillBlockNorthEntity(BlockPos pos, BlockState state) {
		super(QuickieFabricBlockEntity.DRILL_NORTH, pos, state, MAXDRILLSTEP);
	}

	public static void tick(World world, BlockPos pos, BlockState state, BlockEntity be) {
		if (be instanceof DrillBlockNorthEntity) {
			DrillBlockNorthEntity dbe = (DrillBlockNorthEntity) be;
			List<BlockPos> list = new ArrayList<>();
			list.add(pos.north());
			list.add(pos.north().up());
			list.add(pos.north().up().up());
			list.add(pos.north().up().up().up());
			list.add(pos.north().down()); // must be last position
			DrillBlockEntity.tick(world, pos, state, dbe, MAXDRILLSTEP, list);
		}
	}
}
