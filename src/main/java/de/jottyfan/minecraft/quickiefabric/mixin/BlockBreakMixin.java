package de.jottyfan.minecraft.quickiefabric.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import de.jottyfan.minecraft.quickiefabric.event.BreakBlockCallback;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
@Mixin(Block.class)
public class BlockBreakMixin {

	@Inject(at = @At("HEAD"), method = "onBreak(Lnet/minecraft/world/World;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;Lnet/minecraft/entity/player/PlayerEntity;)V")
	private void onBreak(final World world, final BlockPos blockPos, final BlockState blockState, final PlayerEntity playerEntity, final CallbackInfo info) {
		ActionResult result = BreakBlockCallback.EVENT.invoker().injectBlockBreakCallback(world, blockPos, blockState, playerEntity);
		if (result == ActionResult.FAIL) {
			info.cancel();
		}
	}
}
