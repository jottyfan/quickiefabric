package de.jottyfan.minecraft.quickiefabric.event;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public interface BreakBlockCallback {	
	Event<BreakBlockCallback> EVENT = EventFactory.createArrayBacked(BreakBlockCallback.class,
		(listeners) -> (world, blockPos, blockState, playerEntity) -> {
			for (BreakBlockCallback listener : listeners) {
				ActionResult result = listener.injectBlockBreakCallback(world, blockPos, blockState, playerEntity);
				if (result != ActionResult.PASS) {
					return result;
				}
			}
			return ActionResult.PASS;
		});
		
	ActionResult injectBlockBreakCallback(World world, BlockPos blockPos, BlockState blockState, PlayerEntity playerEntity);
}
