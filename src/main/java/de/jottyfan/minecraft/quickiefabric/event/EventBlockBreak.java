package de.jottyfan.minecraft.quickiefabric.event;

import java.util.ArrayList;
import java.util.List;

import de.jottyfan.minecraft.quickiefabric.tools.HarvestRange;
import de.jottyfan.minecraft.quickiefabric.tools.QuickieTools;
import de.jottyfan.minecraft.quickiefabric.tools.ToolRangeable;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class EventBlockBreak {

	private enum BlockBreakDirection {
		UPWARDS, ALL;
	}

	public void doBreakBlock(World world, BlockPos blockPos, BlockState blockState, PlayerEntity playerEntity) {
		ItemStack mainHandItemStack = playerEntity.getEquippedStack(EquipmentSlot.MAINHAND);
		if (mainHandItemStack != null) {
			Item item = mainHandItemStack.getItem();
			if (item instanceof ToolRangeable) {
				ToolRangeable tool = (ToolRangeable) item;
				Block block = blockState.getBlock();
				int handled = handleRangeableTools(tool, mainHandItemStack, world, block, blockPos, playerEntity);
				if (handled >= 255) {
					// reward for using rangeable tool very successful
					world.spawnEntity(
							new ExperienceOrbEntity(world, blockPos.getX(), blockPos.getY(), blockPos.getZ(), handled / 255));
				}
			}
		}
	}

	/**
	 * handle the rangeable tools break event
	 *
	 * @param tool      the tool that has been used
	 * @param itemStack the item stack
	 * @param world     the world
	 * @param block     the block to break
	 * @param pos       the position of the current block
	 * @param player    the current player
	 * @return number of affected blocks
	 */
	private int handleRangeableTools(ToolRangeable tool, ItemStack itemStack, World world, Block currentBlock,
			BlockPos pos, PlayerEntity player) {
		List<Block> validBlocks = tool.getBlockList(currentBlock);
		HarvestRange range = tool.getRange(itemStack);
		if (QuickieTools.SPEEDPOWDERAXE.equals(tool)) {
			return breakBlockRecursive(new ArrayList<>(), world, validBlocks, pos, tool, range, BlockBreakDirection.UPWARDS,
					player, true);
		} else if (QuickieTools.SPEEDPOWDERPICKAXE.equals(tool)) {
			return breakBlockRecursive(new ArrayList<>(), world, validBlocks, pos, tool, range, BlockBreakDirection.ALL,
					player, false);
		} else if (QuickieTools.SPEEDPOWDERSHOVEL.equals(tool)) {
			return breakBlockRecursive(new ArrayList<>(), world, validBlocks, pos, tool, range, BlockBreakDirection.ALL,
					player, false);
		} else if (QuickieTools.SPEEDPOWDERHOE.equals(tool)) {
			return breakBlockRecursive(new ArrayList<>(), world, validBlocks, pos, tool, range, BlockBreakDirection.ALL,
					player, false);
		} else {
			return 0;
		}
	}

	/**
	 * break block recursively;
	 *
	 * @param visitedBlocks       the positions of visited blocks
	 * @param world               the world
	 * @param validBlocks         the blocks to break
	 * @param tool                the tool used
	 * @param range               the range left over
	 * @param pos                 the position
	 * @param blockBreakDirection the direction for the recursive call
	 * @param player              the player
	 * @return number of affected blocks
	 */
	private int breakBlockRecursive(List<String> visitedBlocks, World world, List<Block> validBlocks, BlockPos pos,
			ToolRangeable tool, HarvestRange range, BlockBreakDirection blockBreakDirection, PlayerEntity player,
			boolean breakLeaves) {
		if (visitedBlocks.contains(pos.toString())) {
			return 0;
		} else if (validBlocks == null) {
			return 0;
		} else {
			visitedBlocks.add(pos.toString());
		}
		Integer affected = 0;
		BlockState blockState = world.getBlockState(pos);
		if (tool.canBreakNeighbors(blockState)) {
			Block currentBlock = blockState.getBlock();
			if (validBlocks.contains(currentBlock)) {
				Block.dropStacks(blockState, world, pos); // includes xorbs
				affected += 1;
				world.setBlockState(pos, Blocks.AIR.getDefaultState());
				if (range == null || range.getxRange() > 1 || range.getyRange() > 1 || range.getzRange() > 1) {
					HarvestRange nextRadius = range == null ? null : range.addXYZ(-1);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.north(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.north().east(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.north().west(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.south(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.south().east(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.south().west(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.east(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.west(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.up(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.up().north(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.up().north().east(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.up().north().west(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.up().east(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.up().west(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.up().south().east(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.up().south().west(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);
					affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.up().south(), tool, nextRadius,
							blockBreakDirection, player, breakLeaves);

					if (BlockBreakDirection.ALL.equals(blockBreakDirection)) {
						affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.down(), tool, nextRadius,
								blockBreakDirection, player, breakLeaves);
						affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.down().north(), tool, nextRadius,
								blockBreakDirection, player, breakLeaves);
						affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.down().south(), tool, nextRadius,
								blockBreakDirection, player, breakLeaves);
						affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.down().east(), tool, nextRadius,
								blockBreakDirection, player, breakLeaves);
						affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.down().west(), tool, nextRadius,
								blockBreakDirection, player, breakLeaves);
						affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.down().north().east(), tool,
								nextRadius, blockBreakDirection, player, breakLeaves);
						affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.down().north().west(), tool,
								nextRadius, blockBreakDirection, player, breakLeaves);
						affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.down().south().east(), tool,
								nextRadius, blockBreakDirection, player, breakLeaves);
						affected += breakBlockRecursive(visitedBlocks, world, validBlocks, pos.down().south().west(), tool,
								nextRadius, blockBreakDirection, player, breakLeaves);
					}
				}
			}
		}
		return affected;
	}
}
