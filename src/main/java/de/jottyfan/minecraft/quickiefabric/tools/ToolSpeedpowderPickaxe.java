package de.jottyfan.minecraft.quickiefabric.tools;

import java.util.List;

import com.google.common.collect.Lists;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ToolMaterials;
import net.minecraft.nbt.NbtCompound;

/**
 *
 * @author jotty
 *
 */
public class ToolSpeedpowderPickaxe extends PickaxeItem implements ToolRangeable {

	public static final int[] DEFAULT_HARVEST_RANGE = new int[] { 3, 3, 3 };

	public ToolSpeedpowderPickaxe() {
		super(ToolMaterials.DIAMOND, 4, 2.0f, new FabricItemSettings());
	}

	@Override
	public HarvestRange getRange(ItemStack stack) {
		NbtCompound tag = stack.getNbt();
		int[] range = tag.getIntArray("range");
		if (range.length < 3) {
			range = DEFAULT_HARVEST_RANGE;
			tag.putIntArray("range", range);
		}
		return new HarvestRange(range);
	}

	@Override
	public boolean canBreakNeighbors(BlockState blockIn) {
		return super.isSuitableFor(blockIn);
	}

	@Override
	public List<Block> getBlockList(Block block) {
		return Lists.newArrayList(block);
	}

	public void setPlusRange(ItemStack stack, Integer plusRange) {
		NbtCompound tag = stack.getNbt();
		int[] range = tag.getIntArray("range");
		if (range.length < 3) {
			range = DEFAULT_HARVEST_RANGE;
		}
		HarvestRange harvestRange = new HarvestRange(range);
		harvestRange.addXYZ(plusRange);
		tag.putIntArray("range", harvestRange.getRangeAsArray());
	}

//	@Override
//	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
//		CommonToolCode.onItemRightClick(worldIn, playerIn, handIn);
//		return super.onItemRightClick(worldIn, playerIn, handIn);
//	}
//
//	@Override
//	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
//		CommonToolCode.addInformation(stack, worldIn, tooltip, flagIn);
//		super.addInformation(stack, worldIn, tooltip, flagIn);
//	}
}
