package de.jottyfan.minecraft.quickiefabric.tools;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class ToolSpeedpowderWaterHoe extends ToolSpeedpowderHoe {

	public static final Integer DEFAULT_PLOW_RANGE = 4;
	public HarvestRange range;

	public ToolSpeedpowderWaterHoe() {
		super();
		this.range = new HarvestRange(DEFAULT_PLOW_RANGE);
	}

	@Override
	public ActionResult useOnBlock(ItemUsageContext context) {
		ActionResult res = super.useOnBlock(context);
		if (!ActionResult.PASS.equals(res)) {
			BlockPos pos = context.getBlockPos();
			World world = context.getWorld();
			world.setBlockState(pos, Blocks.WATER.getDefaultState());
			Hand hand = context.getHand();
			PlayerEntity player = context.getPlayer();
			ItemStack oldTool = player.getStackInHand(hand);
			ItemStack newTool = new ItemStack(QuickieTools.SPEEDPOWDERHOE);
			newTool.setDamage(oldTool.getDamage());
			player.setStackInHand(hand, newTool);
		}
		return res;
	}
}
