package de.jottyfan.minecraft.quickiefabric.tools;

import java.util.List;

import com.google.common.collect.Lists;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CropBlock;
import net.minecraft.item.HoeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.item.ToolMaterials;
import net.minecraft.util.ActionResult;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class ToolSpeedpowderHoe extends HoeItem implements ToolRangeable {

	public static final Integer DEFAULT_PLOW_RANGE = 4;
	public HarvestRange range;

	public ToolSpeedpowderHoe() {
		super(ToolMaterials.DIAMOND, 4, 2.0f, new FabricItemSettings());
		this.range = new HarvestRange(DEFAULT_PLOW_RANGE);
	}

	@Override
	public ActionResult useOnBlock(ItemUsageContext context) {
		ActionResult res = super.useOnBlock(context);
		boolean isCrop = context.getWorld().getBlockState(context.getBlockPos()).getBlock() instanceof CropBlock;
		if (!ActionResult.PASS.equals(res) || isCrop) {
			for (int x = -this.range.getxRange(); x <= this.range.getxRange(); x++) {
				for (int y = -this.range.getyRange(); y <= this.range.getyRange(); y++) {
					for (int z = -this.range.getzRange(); z <= this.range.getzRange(); z++) {
						if (!isCrop) {
							BlockHitResult bhr = new BlockHitResult(context.getHitPos(), Direction.UP,
									context.getBlockPos().add(new Vec3i(x, y, z)), isDamageable());
							ItemUsageContext ctx = new ItemUsageContext(context.getPlayer(), context.getHand(), bhr);
							super.useOnBlock(ctx);
						} else {
							harvestIfPossible(context.getBlockPos().add(x, y, z), context.getWorld());
						}
					}
				}
			}
		}
		return res;
	}

	private void harvestIfPossible(BlockPos pos, World world) {
		BlockState blockState = world.getBlockState(pos);
		Block block = blockState.getBlock();
		if (block instanceof CropBlock) {
			CropBlock cBlock = (CropBlock) block;
			if (cBlock.isMature(blockState)) {
				Block.dropStacks(blockState, world, pos);
				world.setBlockState(pos, cBlock.withAge(0));
			}
		}
	}

	@Override
	public HarvestRange getRange(ItemStack stack) {
		// TODO: get range from stack
		return range;
	}

	@Override
	public boolean canBreakNeighbors(BlockState blockState) {
		return super.isSuitableFor(blockState)
				|| Blocks.GRASS.equals(blockState.getBlock())
				|| Blocks.FERN.equals(blockState.getBlock())
				|| Blocks.LARGE_FERN.equals(blockState.getBlock());
	}


	@Override
	public List<Block> getBlockList(Block block) {
		return Lists.newArrayList(block);
	}

//	@Override
//	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
//		CommonToolCode.onItemRightClick(worldIn, playerIn, handIn);
//		return super.onItemRightClick(worldIn, playerIn, handIn);
//	}
//
//	@Override
//	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
//		CommonToolCode.addInformation(stack, worldIn, tooltip, flagIn);
//		super.addInformation(stack, worldIn, tooltip, flagIn);
//	}
}
