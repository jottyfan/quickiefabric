package de.jottyfan.minecraft.quickiefabric.tools;

/**
 *
 * @author jotty
 *
 */
public class QuickieTools {
	public static final ToolSpeedpowderAxe SPEEDPOWDERAXE = new ToolSpeedpowderAxe();
	public static final ToolSpeedpowderPickaxe SPEEDPOWDERPICKAXE = new ToolSpeedpowderPickaxe();
	public static final ToolSpeedpowderShovel SPEEDPOWDERSHOVEL = new ToolSpeedpowderShovel();
	public static final ToolSpeedpowderHoe SPEEDPOWDERHOE = new ToolSpeedpowderHoe();
	public static final ToolSpeedpowderWaterHoe SPEEDPOWDERWATERHOE = new ToolSpeedpowderWaterHoe();
}
