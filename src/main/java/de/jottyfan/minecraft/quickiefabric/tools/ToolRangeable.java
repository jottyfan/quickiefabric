package de.jottyfan.minecraft.quickiefabric.tools;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;

/**
 *
 * @author jotty
 *
 */
public interface ToolRangeable {

	/**
	 * @param stack the item stack that keeps the range
	 * @return range of blocks to be harvested
	 */
	public HarvestRange getRange(ItemStack stack);

	/**
	 * check if this block state is one that affects the neighbor blocks to break
	 * also if they are from the same type
	 *
	 * @param blockState the block state of the current block
	 * @return true or false
	 */
	public boolean canBreakNeighbors(BlockState blockState);

	/**
	 * get list of blocks that belong together (could be useful for stripped logs)
	 *
	 * @param block of the set
	 * @return the list of blocks or null if not found
	 */
	public List<Block> getBlockList(Block block);
}
