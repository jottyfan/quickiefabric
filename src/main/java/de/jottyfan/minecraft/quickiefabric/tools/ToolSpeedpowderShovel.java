package de.jottyfan.minecraft.quickiefabric.tools;

import java.util.List;

import com.google.common.collect.Lists;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.item.ShovelItem;
import net.minecraft.item.ToolMaterials;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class ToolSpeedpowderShovel extends ShovelItem implements ToolRangeable {
	public static final Integer DEFAULT_HARVEST_RANGE = 3;
	public HarvestRange range;

	public ToolSpeedpowderShovel() {
		super(ToolMaterials.DIAMOND, 4, 2.0f, new FabricItemSettings());
		this.range = new HarvestRange(DEFAULT_HARVEST_RANGE);
	}

	private void createPathOnGrass(World world, BlockPos pos, Direction side) {
		BlockState blockState = world.getBlockState(pos);
		if (blockState.isAir()) {
			// try to find one underneath
			pos = pos.down();
			blockState = world.getBlockState(pos);
		} else if (!world.getBlockState(pos.up()).isAir()) {
			pos = pos.up();
			blockState = world.getBlockState(pos);
		}
		if (side != Direction.DOWN) {
			BlockState blockState2 = (BlockState) PATH_STATES.get(blockState.getBlock());
			if (blockState2 != null && world.getBlockState(pos.up()).isAir()) {
				if (!world.isClient) {
					world.setBlockState(pos, blockState2, 11);
				}
			}
		}
	}

	@Override
	public ActionResult useOnBlock(ItemUsageContext context) {
		World world = context.getWorld();
		BlockPos pos = context.getBlockPos();
		createPathOnGrass(world, pos.north(), context.getSide());
		createPathOnGrass(world, pos.north().east(), context.getSide());
		createPathOnGrass(world, pos.north().west(), context.getSide());
		createPathOnGrass(world, pos.east(), context.getSide());
		createPathOnGrass(world, pos.west(), context.getSide());
		createPathOnGrass(world, pos.south(), context.getSide());
		createPathOnGrass(world, pos.south().east(), context.getSide());
		createPathOnGrass(world, pos.south().west(), context.getSide());
		return super.useOnBlock(context);
	}

	@Override
	public HarvestRange getRange(ItemStack stack) {
		// TODO: get range from stack
		return range;
	}

	@Override
	public boolean canBreakNeighbors(BlockState blockState) {
		return super.isSuitableFor(blockState);
	}

	@Override
	public List<Block> getBlockList(Block block) {
		return Lists.newArrayList(block);
	}

//	@Override
//	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
//		CommonToolCode.onItemRightClick(worldIn, playerIn, handIn);
//		return super.onItemRightClick(worldIn, playerIn, handIn);
//	}
//
//	@Override
//	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
//		CommonToolCode.addInformation(stack, worldIn, tooltip, flagIn);
//		super.addInformation(stack, worldIn, tooltip, flagIn);
//	}
}
