package de.jottyfan.minecraft.quickiefabric.tools;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.ToolMaterials;

/**
 *
 * @author jotty
 *
 */
public class ToolSpeedpowderAxe extends ToolRangeableAxe {

	public ToolSpeedpowderAxe() {
		super(ToolMaterials.DIAMOND, 4, 2.0f, new FabricItemSettings());
	}
}
