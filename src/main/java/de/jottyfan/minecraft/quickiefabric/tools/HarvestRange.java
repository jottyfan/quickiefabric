package de.jottyfan.minecraft.quickiefabric.tools;

import java.io.Serializable;

/**
 * 
 * @author jotty
 *
 */
public class HarvestRange implements Serializable {
	private static final long serialVersionUID = 1L;
	private int xRange;
	private int yRange;
	private int zRange;

	public HarvestRange(int xyzRange) {
		super();
		this.xRange = xyzRange;
		this.yRange = xyzRange;
		this.zRange = xyzRange;
	}
	
	public HarvestRange(int[] xyzRange) {
		super();
		this.xRange = xyzRange[0];
		this.yRange = xyzRange[1];
		this.zRange = xyzRange[2];
	}

	public HarvestRange(int xRange, int yRange, int zRange) {
		super();
		this.xRange = xRange;
		this.yRange = yRange;
		this.zRange = zRange;
	}

	/**
	 * add i to x, y and z and return the resulting class as a new one
	 * 
	 * @param i
	 *          the summand
	 * @return the new class
	 */
	public HarvestRange addXYZ(int i) {
		return new HarvestRange(xRange + i, yRange + i, zRange + i);
	}
	
	/**
	 * get range as int array
	 * 
	 * @return the int array
	 */
	public int[] getRangeAsArray() {
		return new int[] {xRange, yRange, zRange};
	}

	/**
	 * @return the xRange
	 */
	public int getxRange() {
		return xRange;
	}

	/**
	 * @param xRange
	 *          the xRange to set
	 */
	public void setxRange(int xRange) {
		this.xRange = xRange;
	}

	/**
	 * @return the yRange
	 */
	public int getyRange() {
		return yRange;
	}

	/**
	 * @param yRange
	 *          the yRange to set
	 */
	public void setyRange(int yRange) {
		this.yRange = yRange;
	}

	/**
	 * @return the zRange
	 */
	public int getzRange() {
		return zRange;
	}

	/**
	 * @param zRange
	 *          the zRange to set
	 */
	public void setzRange(int zRange) {
		this.zRange = zRange;
	}
}
