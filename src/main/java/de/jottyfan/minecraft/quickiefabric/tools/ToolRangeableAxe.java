package de.jottyfan.minecraft.quickiefabric.tools;

import java.util.List;

import com.google.common.collect.Lists;
import com.terraformersmc.terraform.leaves.block.ExtendedLeavesBlock;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.LeavesBlock;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolMaterial;
import net.minecraft.registry.tag.BlockTags;

/**
 *
 * @author jotty
 *
 */
public class ToolRangeableAxe extends AxeItem implements ToolRangeable {

	protected ToolRangeableAxe(ToolMaterial material, float attachDamage, float attackSpeedIn, Settings settings) {
		super(material, attachDamage, attackSpeedIn, settings);
	}

	@Override
	public HarvestRange getRange(ItemStack stack) {
		// TODO: get the range from the stack
		return new HarvestRange(64, 128, 64); // trees bigger than that are too heavy for one small axe...
	}

	@Override
	public float getMiningSpeedMultiplier(ItemStack stack, BlockState state) {
		return this.miningSpeed;
	}

	/**
	 * check if the block is a leaves block
	 *
	 * @param blockIn the block
	 * @return true or false
	 */
	private boolean isLeavesBlock(BlockState blockIn) {
		boolean vanillaLeaves = blockIn.getBlock() instanceof LeavesBlock;
		boolean terrestriaLeaves = blockIn.getBlock() instanceof ExtendedLeavesBlock;
		boolean blockTagLeaves = blockIn.isIn(BlockTags.LEAVES);
		return vanillaLeaves || terrestriaLeaves || blockTagLeaves;
	}

	@Override
	public boolean canBreakNeighbors(BlockState blockIn) {
		return super.isSuitableFor(blockIn) || isLeavesBlock(blockIn);
	}

	@Override
	public List<Block> getBlockList(Block block) {
		return Lists.newArrayList(block);
	}
}
