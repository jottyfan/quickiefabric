package de.jottyfan.minecraft.quickiefabric.init;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.jottyfan.minecraft.quickiefabric.blockentity.BlockSpreaderEntity;
import de.jottyfan.minecraft.quickiefabric.blockentity.BlockStackerEntity;
import de.jottyfan.minecraft.quickiefabric.blockentity.DrillBlockDownEntity;
import de.jottyfan.minecraft.quickiefabric.blockentity.DrillBlockEastEntity;
import de.jottyfan.minecraft.quickiefabric.blockentity.DrillBlockNorthEntity;
import de.jottyfan.minecraft.quickiefabric.blockentity.DrillBlockSouthEntity;
import de.jottyfan.minecraft.quickiefabric.blockentity.DrillBlockWestEntity;
import de.jottyfan.minecraft.quickiefabric.blockentity.EmptyLavaHoarderBlockEntity;
import de.jottyfan.minecraft.quickiefabric.blockentity.ItemHoarderBlockEntity;
import de.jottyfan.minecraft.quickiefabric.blockentity.MonsterHoarderBlockEntity;
import de.jottyfan.minecraft.quickiefabric.blockentity.QuickieFabricBlockEntity;
import de.jottyfan.minecraft.quickiefabric.blocks.QuickieBlocks;
import de.jottyfan.minecraft.quickiefabric.container.BackpackScreenHandler;
import de.jottyfan.minecraft.quickiefabric.container.BlockStackerScreenHandler;
import de.jottyfan.minecraft.quickiefabric.event.BreakBlockCallback;
import de.jottyfan.minecraft.quickiefabric.event.EventBlockBreak;
import de.jottyfan.minecraft.quickiefabric.items.QuickieItems;
import de.jottyfan.minecraft.quickiefabric.loot.LootHelper;
import de.jottyfan.minecraft.quickiefabric.tools.QuickieTools;
import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.fabricmc.fabric.api.biome.v1.ModificationPhase;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.fabricmc.fabric.api.loot.v2.LootTableEvents;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.registry.FuelRegistry;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.ComposterBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;

/**
 *
 * @author jotty
 *
 */
public class RegistryManager {
	private static final Logger LOGGER = LogManager.getLogger(RegistryManager.class);

	public static final String QUICKIEFABRIC = "quickiefabric";

	public static final Identifier BACKPACK_IDENTIFIER = new Identifier(QUICKIEFABRIC, "backpack");
	public static final ScreenHandlerType<BackpackScreenHandler> BACKPACK_SCREEN_HANDLER = ScreenHandlerRegistry
			.registerExtended(RegistryManager.BACKPACK_IDENTIFIER, BackpackScreenHandler::new);
	public static final Identifier STACKER_IDENTIFIER = new Identifier(QUICKIEFABRIC, "stacker");
	public static final ScreenHandlerType<BlockStackerScreenHandler> BLOCKSTACKER_SCREEN_HANDLER = ScreenHandlerRegistry
			.registerSimple(RegistryManager.STACKER_IDENTIFIER, BlockStackerScreenHandler::new);

	public static final RegistryKey<ItemGroup> QUICKIEFABRIC_GROUP = RegistryKey.of(RegistryKeys.ITEM_GROUP,
			new Identifier(QUICKIEFABRIC, "itemgroups"));

	public static final void registerItemGroup() {
		Registry.register(Registries.ITEM_GROUP, QUICKIEFABRIC_GROUP,
				FabricItemGroup.builder().icon(() -> new ItemStack(QuickieItems.SPEEDPOWDER))
						.displayName(Text.literal(QUICKIEFABRIC)).entries((enabledFeatures, stacks) -> {
							stacks.add(new ItemStack(QuickieItems.SALPETER));
							stacks.add(new ItemStack(QuickieItems.SULPHOR));
							stacks.add(new ItemStack(QuickieItems.SPEEDPOWDER));
							stacks.add(new ItemStack(QuickieItems.LEVELUP));
							stacks.add(new ItemStack(QuickieItems.PENCIL));
							stacks.add(new ItemStack(QuickieItems.ROTTEN_FLESH_STRIPES));
							stacks.add(new ItemStack(QuickieItems.CARROTSTACK));
							stacks.add(new ItemStack(QuickieItems.COTTON));
							stacks.add(new ItemStack(QuickieItems.COTTONSEED));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_BROWN));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_WHITE));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_BLACK));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_BLUE));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_CYAN));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_GREEN));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_PINK));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_RED));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_YELLOW));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_DARKGRAY));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_LIGHTGRAY));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_LIGHTGREEN));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_MAGENTA));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_ORANGE));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_PURPLE));
							stacks.add(new ItemStack(QuickieItems.BACKPACK_LIGHTBLUE));
							stacks.add(new ItemStack(QuickieItems.BAG));
							stacks.add(new ItemStack(QuickieItems.STUB));
							stacks.add(new ItemStack(QuickieTools.SPEEDPOWDERAXE));
							stacks.add(new ItemStack(QuickieTools.SPEEDPOWDERPICKAXE));
							stacks.add(new ItemStack(QuickieTools.SPEEDPOWDERSHOVEL));
							stacks.add(new ItemStack(QuickieTools.SPEEDPOWDERHOE));
							stacks.add(new ItemStack(QuickieTools.SPEEDPOWDERWATERHOE));
							stacks.add(new ItemStack(QuickieBlocks.DIRT_SALPETER));
							stacks.add(new ItemStack(QuickieBlocks.ORE_NETHER_SULPHOR));
							stacks.add(new ItemStack(QuickieBlocks.ORE_SALPETER));
							stacks.add(new ItemStack(QuickieBlocks.ORE_SAND_SALPETER));
							stacks.add(new ItemStack(QuickieBlocks.ORE_SULPHOR));
							stacks.add(new ItemStack(QuickieBlocks.ORE_DEEPSLATESULPHOR));
							stacks.add(new ItemStack(QuickieBlocks.SAND_SALPETER));
							stacks.add(new ItemStack(QuickieBlocks.LAVAHOARDER));
							stacks.add(new ItemStack(QuickieBlocks.EMPTYLAVAHOARDER));
							stacks.add(new ItemStack(QuickieBlocks.ITEMHOARDER));
							stacks.add(new ItemStack(QuickieBlocks.MONSTERHOARDER));
							stacks.add(new ItemStack(QuickieBlocks.KELPSTACK));
							stacks.add(new ItemStack(QuickieBlocks.BLOCKSULPHOR));
							stacks.add(new ItemStack(QuickieBlocks.BLOCKSALPETER));
							stacks.add(new ItemStack(QuickieBlocks.DRILL_DOWN));
							stacks.add(new ItemStack(QuickieBlocks.DRILL_EAST));
							stacks.add(new ItemStack(QuickieBlocks.DRILL_SOUTH));
							stacks.add(new ItemStack(QuickieBlocks.DRILL_WEST));
							stacks.add(new ItemStack(QuickieBlocks.DRILL_NORTH));
							stacks.add(new ItemStack(QuickieBlocks.DRILLSTOP));
							stacks.add(new ItemStack(QuickieBlocks.BLOCKSTACKERUP));
							stacks.add(new ItemStack(QuickieBlocks.BLOCKSTACKERDOWN));
							stacks.add(new ItemStack(QuickieBlocks.BLOCKSTACKEREAST));
							stacks.add(new ItemStack(QuickieBlocks.BLOCKSTACKERWEST));
							stacks.add(new ItemStack(QuickieBlocks.BLOCKSTACKERNORTH));
							stacks.add(new ItemStack(QuickieBlocks.BLOCKSTACKERSOUTH));
							stacks.add(new ItemStack(QuickieBlocks.BLOCKSPREADER));
						}).build());
	}

	private static final void registerBlock(Block block, String name) {
		Registry.register(Registries.BLOCK, new Identifier(QUICKIEFABRIC, name), block);
		Registry.register(Registries.ITEM, new Identifier(QUICKIEFABRIC, name),
				new BlockItem(block, new FabricItemSettings()));
	}

	private static final void registerItem(Item item, String name) {
		Registry.register(Registries.ITEM, new Identifier(QUICKIEFABRIC, name), item);
	}

	public static final <T extends BlockEntity> BlockEntityType<? extends T> registerBlockEntity(String name,
			FabricBlockEntityTypeBuilder.Factory<? extends T> supplier, Block... blocks) {
		String fullname = new StringBuilder().append(QUICKIEFABRIC).append(":").append(name).toString();
		FabricBlockEntityTypeBuilder<? extends T> builder = FabricBlockEntityTypeBuilder.create(supplier, blocks);
		BlockEntityType<? extends T> blockEntityType = builder.build(null);
		return Registry.register(Registries.BLOCK_ENTITY_TYPE, fullname, blockEntityType);
	}

	@SuppressWarnings("unchecked")
	public static final void registerBlockEntities() {
		LOGGER.debug("registering quickiefabric block entities");
		QuickieFabricBlockEntity.ITEMHOARDER = (BlockEntityType<ItemHoarderBlockEntity>) registerBlockEntity(
				"itemhoarderblockentity", ItemHoarderBlockEntity::new, QuickieBlocks.ITEMHOARDER);
		QuickieFabricBlockEntity.MONSTERHOARDER = (BlockEntityType<MonsterHoarderBlockEntity>) registerBlockEntity(
				"monsterhoarderblockentity", MonsterHoarderBlockEntity::new, QuickieBlocks.MONSTERHOARDER);
		QuickieFabricBlockEntity.EMPTYLAVAHOARDER = (BlockEntityType<EmptyLavaHoarderBlockEntity>) registerBlockEntity(
				"emptylavahoarderblockentity", EmptyLavaHoarderBlockEntity::new, QuickieBlocks.EMPTYLAVAHOARDER,
				QuickieBlocks.LAVAHOARDER);
		QuickieFabricBlockEntity.DRILL_DOWN = (BlockEntityType<DrillBlockDownEntity>) registerBlockEntity(
				"drillblockdownentity", DrillBlockDownEntity::new, QuickieBlocks.DRILL_DOWN);
		QuickieFabricBlockEntity.DRILL_EAST = (BlockEntityType<DrillBlockEastEntity>) registerBlockEntity(
				"drillblockeastentity", DrillBlockEastEntity::new, QuickieBlocks.DRILL_EAST);
		QuickieFabricBlockEntity.DRILL_SOUTH = (BlockEntityType<DrillBlockSouthEntity>) registerBlockEntity(
				"drillblocksouthentity", DrillBlockSouthEntity::new, QuickieBlocks.DRILL_SOUTH);
		QuickieFabricBlockEntity.DRILL_WEST = (BlockEntityType<DrillBlockWestEntity>) registerBlockEntity(
				"drillblockwestentity", DrillBlockWestEntity::new, QuickieBlocks.DRILL_WEST);
		QuickieFabricBlockEntity.DRILL_NORTH = (BlockEntityType<DrillBlockNorthEntity>) registerBlockEntity(
				"drillblocknorthentity", DrillBlockNorthEntity::new, QuickieBlocks.DRILL_NORTH);
		QuickieFabricBlockEntity.BLOCKSTACKER_ENTITY = (BlockEntityType<BlockStackerEntity>) registerBlockEntity(
				"blockstackerentity", BlockStackerEntity::new, QuickieBlocks.BLOCKSTACKERUP, QuickieBlocks.BLOCKSTACKERDOWN,
				QuickieBlocks.BLOCKSTACKEREAST, QuickieBlocks.BLOCKSTACKERWEST, QuickieBlocks.BLOCKSTACKERNORTH,
				QuickieBlocks.BLOCKSTACKERSOUTH);
		QuickieFabricBlockEntity.BLOCKSPREADER_ENTITY = (BlockEntityType<BlockSpreaderEntity>) registerBlockEntity(
				"blockspreaderentity", BlockSpreaderEntity::new, QuickieBlocks.BLOCKSPREADER);
	}

	public static final void registerBlocks() {
		LOGGER.debug("registering quickiefabric blocks");
		registerBlock(QuickieBlocks.DIRT_SALPETER, "dirtsalpeter");
		registerBlock(QuickieBlocks.ORE_NETHER_SULPHOR, "orenethersulphor");
		registerBlock(QuickieBlocks.ORE_SALPETER, "oresalpeter");
		registerBlock(QuickieBlocks.ORE_SAND_SALPETER, "oresandsalpeter");
		registerBlock(QuickieBlocks.ORE_SULPHOR, "oresulphor");
		registerBlock(QuickieBlocks.ORE_DEEPSLATESULPHOR, "oredeepslatesulphor");
		registerBlock(QuickieBlocks.SAND_SALPETER, "sandsalpeter");
		registerBlock(QuickieBlocks.LAVAHOARDER, "lavahoarder");
		registerBlock(QuickieBlocks.EMPTYLAVAHOARDER, "emptylavahoarder");
		registerBlock(QuickieBlocks.ITEMHOARDER, "itemhoarder");
		registerBlock(QuickieBlocks.MONSTERHOARDER, "monsterhoarder");
		registerBlock(QuickieBlocks.KELPSTACK, "kelpstack");
		registerBlock(QuickieBlocks.COTTONPLANT, "cottonplant");
		registerBlock(QuickieBlocks.BLOCKSULPHOR, "blocksulphor");
		registerBlock(QuickieBlocks.BLOCKSALPETER, "blocksalpeter");
		registerBlock(QuickieBlocks.DRILL_DOWN, "drill");
		registerBlock(QuickieBlocks.DRILL_EAST, "drilleast");
		registerBlock(QuickieBlocks.DRILL_SOUTH, "drillsouth");
		registerBlock(QuickieBlocks.DRILL_WEST, "drillwest");
		registerBlock(QuickieBlocks.DRILL_NORTH, "drillnorth");
		registerBlock(QuickieBlocks.DRILLSTOP, "drillstop");
		registerBlock(QuickieBlocks.BLOCKSTACKERUP, "blockstackerup");
		registerBlock(QuickieBlocks.BLOCKSTACKERDOWN, "blockstackerdown");
		registerBlock(QuickieBlocks.BLOCKSTACKEREAST, "blockstackereast");
		registerBlock(QuickieBlocks.BLOCKSTACKERWEST, "blockstackerwest");
		registerBlock(QuickieBlocks.BLOCKSTACKERNORTH, "blockstackernorth");
		registerBlock(QuickieBlocks.BLOCKSTACKERSOUTH, "blockstackersouth");
		registerBlock(QuickieBlocks.BLOCKSPREADER, "blockspreader");
	}

	public static final void registerItems() {
		LOGGER.debug("registering quickiefabric items");
		registerItem(QuickieItems.SPEEDPOWDER, "speedpowder");
		registerItem(QuickieItems.LEVELUP, "levelup");
		registerItem(QuickieItems.PENCIL, "pencil");
		registerItem(QuickieItems.SALPETER, "salpeter");
		registerItem(QuickieItems.SULPHOR, "sulphor");
		registerItem(QuickieItems.ROTTEN_FLESH_STRIPES, "rotten_flesh_stripes");
		registerItem(QuickieItems.CARROTSTACK, "carrotstack");
		registerItem(QuickieItems.COTTON, "cotton");
		registerItem(QuickieItems.COTTONSEED, "cottonseed");
		registerItem(QuickieItems.BACKPACK_BROWN, "backpack_brown");
		registerItem(QuickieItems.BACKPACK_WHITE, "backpack_white");
		registerItem(QuickieItems.BACKPACK_BLACK, "backpack_black");
		registerItem(QuickieItems.BACKPACK_BLUE, "backpack_blue");
		registerItem(QuickieItems.BACKPACK_CYAN, "backpack_cyan");
		registerItem(QuickieItems.BACKPACK_GREEN, "backpack_green");
		registerItem(QuickieItems.BACKPACK_PINK, "backpack_pink");
		registerItem(QuickieItems.BACKPACK_RED, "backpack_red");
		registerItem(QuickieItems.BACKPACK_YELLOW, "backpack_yellow");
		registerItem(QuickieItems.BACKPACK_DARKGRAY, "backpack_darkgray");
		registerItem(QuickieItems.BACKPACK_LIGHTGRAY, "backpack_lightgray");
		registerItem(QuickieItems.BACKPACK_LIGHTGREEN, "backpack_lightgreen");
		registerItem(QuickieItems.BACKPACK_MAGENTA, "backpack_magenta");
		registerItem(QuickieItems.BACKPACK_ORANGE, "backpack_orange");
		registerItem(QuickieItems.BACKPACK_PURPLE, "backpack_purple");
		registerItem(QuickieItems.BACKPACK_LIGHTBLUE, "backpack_lightblue");
		registerItem(QuickieItems.BAG, "bag");
		registerItem(QuickieItems.STUB, "stub");

		ComposterBlock.ITEM_TO_LEVEL_INCREASE_CHANCE.put(QuickieItems.COTTONSEED, 0.5f);
		ComposterBlock.ITEM_TO_LEVEL_INCREASE_CHANCE.put(QuickieItems.COTTON, 0.75f);

		FuelRegistry.INSTANCE.add(QuickieItems.SULPHOR, 200);
		FuelRegistry.INSTANCE.add(QuickieBlocks.BLOCKSULPHOR, 2000);
		FuelRegistry.INSTANCE.add(Blocks.MAGMA_BLOCK, 5000);
	}

	public static final void registerTools() {
		LOGGER.debug("registering quickiefabric tools");
		registerItem(QuickieTools.SPEEDPOWDERAXE, "speedpowderaxe");
		registerItem(QuickieTools.SPEEDPOWDERPICKAXE, "speedpowderpickaxe");
		registerItem(QuickieTools.SPEEDPOWDERSHOVEL, "speedpowdershovel");
		registerItem(QuickieTools.SPEEDPOWDERHOE, "speedpowderhoe");
		registerItem(QuickieTools.SPEEDPOWDERWATERHOE, "speedpowderwaterhoe");
	}

	public static final void registerContainer() {
	}

	public static final void registerEvents() {
		LOGGER.debug("registering quickiefabric events");
		BreakBlockCallback.EVENT.register((world, blockPos, blockState, playerEntity) -> {
			new EventBlockBreak().doBreakBlock(world, blockPos, blockState, playerEntity);
			return ActionResult.SUCCESS;
		});
	}

	/**
	 * register ore generation features
	 */
	public static final void registerFeatures() {
		// Overworld features
		BiomeModifications.create(new Identifier(QUICKIEFABRIC, "features")).add(ModificationPhase.ADDITIONS,
				BiomeSelectors.foundInOverworld(), FeaturesManager.overworldOres());

		// Nether features
		BiomeModifications.create(new Identifier(QUICKIEFABRIC, "nether_features")).add(ModificationPhase.ADDITIONS,
				BiomeSelectors.foundInTheNether(), FeaturesManager.netherOres());
	}

	public static final void registerLootings() {
		LootTableEvents.MODIFY.register((resourceManager, lootManager, id, tableBuilder, source) -> {
			if (isGrass(id)) {
				tableBuilder.pool(LootHelper.build(1, QuickieItems.COTTONSEED, 0.125f));
				tableBuilder.pool(LootHelper.build(2, QuickieItems.SALPETER, 0.012f));
			}
		});
	}

	private final static boolean isGrass(Identifier id) {
		return id.equals(new Identifier("blocks/grass")) || id.equals(new Identifier("blocks/fern"))
				|| id.equals(new Identifier("blocks/tall_grass"));
	}

	public final static void registerTags() {
		TagKey<Item> BLOCKSTACKER = TagKey.of(RegistryKeys.ITEM, new Identifier(QUICKIEFABRIC, "blockstacker"));
	}
}
