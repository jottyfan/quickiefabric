package de.jottyfan.minecraft.quickiefabric.init;

import java.util.function.BiConsumer;

import net.fabricmc.fabric.api.biome.v1.BiomeModificationContext;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectionContext;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.util.Identifier;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.PlacedFeature;

/**
 *
 * @author jotty
 *
 */
public class FeaturesManager {

  public static final RegistryKey<ConfiguredFeature<?, ?>> CF_ORESULFUR = genCf("oresulphor");
  public static final RegistryKey<ConfiguredFeature<?, ?>> CF_OREDEEPSLATESULFUR = genCf("oredepslatesulphor");
  public static final RegistryKey<ConfiguredFeature<?, ?>> CF_ORESALPETER = genCf("oresalpeter");
  public static final RegistryKey<ConfiguredFeature<?, ?>> CF_ORENETHERSULPHOR = genCf("orenethersulphore");
  public static final RegistryKey<ConfiguredFeature<?, ?>> CF_BLOCKSULPHOR = genCf("blocksulphor");
  public static final RegistryKey<ConfiguredFeature<?, ?>> CF_DIRTSALPETER = genCf("dirtsalpeter");
  public static final RegistryKey<ConfiguredFeature<?, ?>> CF_SANDSALPETER = genCf("sandsalpeter");
  public static final RegistryKey<ConfiguredFeature<?, ?>> CF_ORESANDSALPETER = genCf("oresandsalpeter");

  public static final RegistryKey<PlacedFeature> PF_ORESULPHOR  = genPf("oresulphor");
  public static final RegistryKey<PlacedFeature> PF_OREDEEPSLATESULPHOR  = genPf("oredeepslatesulphor");
  public static final RegistryKey<PlacedFeature> PF_ORESALPETER  = genPf("oresalpeter");
  public static final RegistryKey<PlacedFeature> PF_ORENETHERSULPHOR  = genPf("orenethersulphor");
  public static final RegistryKey<PlacedFeature> PF_BLOCKSULPHOR  = genPf("blocksulphor");
  public static final RegistryKey<PlacedFeature> PF_DIRTSALPETER  = genPf("dirtsalpeter");
  public static final RegistryKey<PlacedFeature> PF_SANDSALPETER  = genPf("sandsalpeter");
  public static final RegistryKey<PlacedFeature> PF_ORESANDSALPETER  = genPf("oresandsalpeter");

  private static final RegistryKey<ConfiguredFeature<?, ?>> genCf(String name) {
  	return RegistryKey.of(RegistryKeys.CONFIGURED_FEATURE, new Identifier(RegistryManager.QUICKIEFABRIC, name));
  }

  private static final RegistryKey<PlacedFeature> genPf(String name) {
  	return RegistryKey.of(RegistryKeys.PLACED_FEATURE, new Identifier(RegistryManager.QUICKIEFABRIC, name));
  }

	protected static final BiConsumer<BiomeSelectionContext, BiomeModificationContext> overworldOres() {
		return (bsc, bmc) -> {
			bmc.getGenerationSettings().addFeature(GenerationStep.Feature.UNDERGROUND_ORES, PF_ORESULPHOR);
			bmc.getGenerationSettings().addFeature(GenerationStep.Feature.UNDERGROUND_ORES, PF_ORESALPETER);
			bmc.getGenerationSettings().addFeature(GenerationStep.Feature.UNDERGROUND_ORES, PF_DIRTSALPETER);
			bmc.getGenerationSettings().addFeature(GenerationStep.Feature.UNDERGROUND_ORES, PF_SANDSALPETER);
			bmc.getGenerationSettings().addFeature(GenerationStep.Feature.UNDERGROUND_ORES, PF_ORESANDSALPETER);
			bmc.getGenerationSettings().addFeature(GenerationStep.Feature.UNDERGROUND_ORES, PF_OREDEEPSLATESULPHOR);
		};
	}

	protected static final BiConsumer<BiomeSelectionContext, BiomeModificationContext> netherOres() {
		return (bsc, bmc) -> {
			bmc.getGenerationSettings().addFeature(GenerationStep.Feature.UNDERGROUND_ORES, PF_ORENETHERSULPHOR);
			bmc.getGenerationSettings().addFeature(GenerationStep.Feature.UNDERGROUND_ORES, PF_BLOCKSULPHOR);
		};
	}
}
