package de.jottyfan.minecraft.quickiefabric;

import de.jottyfan.minecraft.quickiefabric.blocks.QuickieBlocks;
import de.jottyfan.minecraft.quickiefabric.container.BackpackScreen;
import de.jottyfan.minecraft.quickiefabric.container.BlockStackerScreen;
import de.jottyfan.minecraft.quickiefabric.init.RegistryManager;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.client.render.RenderLayer;

/**
 *
 * @author jotty
 *
 */
@Environment(EnvType.CLIENT)
public class QuickieFabricClient implements ClientModInitializer {
	@Override
	public void onInitializeClient() {
		HandledScreens.register(RegistryManager.BACKPACK_SCREEN_HANDLER, BackpackScreen::new);
		HandledScreens.register(RegistryManager.BLOCKSTACKER_SCREEN_HANDLER, BlockStackerScreen::new);
		// make cotton plant block transparent
		BlockRenderLayerMap.INSTANCE.putBlock(QuickieBlocks.COTTONPLANT, RenderLayer.getCutout());
	}
}
