package de.jottyfan.minecraft.quickiefabric.api;

import java.util.Objects;

import net.minecraft.util.math.BlockPos;

/**
 * 
 * @author jotty
 *
 */
public class NeighborhoodBean {
	private final BlockPos pos;
	private boolean checked;

	public NeighborhoodBean(BlockPos pos, boolean checked) {
		super();
		this.pos = pos;
		this.checked = checked;
	}

	public NeighborhoodBean(BlockPos pos) {
		super();
		this.pos = pos;
		this.checked = false;
	}

	public NeighborhoodBean over() {
		return new NeighborhoodBean(pos.up(), checked);
	}

	public NeighborhoodBean below() {
		return new NeighborhoodBean(pos.down(), checked);
	}

	@Override
	public int hashCode() {
		return Objects.hash(pos);
	}

	@Override
	public boolean equals(Object obj) {
		Boolean result = null;
		if (this == obj) {
			result = true;
		} else if (obj == null) {
			result = false;
		} else if (getClass() != obj.getClass()) {
			result = false;
		} else {
			NeighborhoodBean other = (NeighborhoodBean) obj;
			result = Objects.equals(pos, other.pos);
		}
		return result;
	}

	/**
	 * @return the checked
	 */
	public boolean isChecked() {
		return checked;
	}

	/**
	 * @param checked the checked to set
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	/**
	 * @return the pos
	 */
	public BlockPos getPos() {
		return pos;
	}
}
