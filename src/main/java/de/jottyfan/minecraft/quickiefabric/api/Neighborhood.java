package de.jottyfan.minecraft.quickiefabric.api;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.BiFunction;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public class Neighborhood {

	/**
	 * find the same blocks that is next to the current position pos
	 * 
	 * @param world       the world to look for the blocks
	 * @param pos         the starting position
	 * @param seekLimit   the limit of seek operations
	 * @param checkLambda check functionality
	 * @return a set of found block positions
	 */
	public static final Set<BlockPos> findEqualBlock(World world, BlockPos pos, int seekLimit,
			BiFunction<World, BlockPos, Boolean> checkLambda) {
		Set<NeighborhoodBean> found = new HashSet<>();
		found.add(new NeighborhoodBean(pos, true));

		while (pos != null && found.size() < seekLimit) {
			findNewNeihgbor(world, pos.east(), found, checkLambda);
			findNewNeihgbor(world, pos.south(), found, checkLambda);
			findNewNeihgbor(world, pos.west(), found, checkLambda);
			findNewNeihgbor(world, pos.north(), found, checkLambda);
			pos = findNextUncheckedField(found);
		}

		Set<BlockPos> finals = new HashSet<>();
		for (NeighborhoodBean bean : found) {
			finals.add(bean.getPos());
		}
		return finals;
	}

	private static final BlockPos findNextUncheckedField(Set<NeighborhoodBean> found) {
		Iterator<NeighborhoodBean> i = found.iterator();
		while (i.hasNext()) {
			NeighborhoodBean bean = i.next();
			if (!bean.isChecked()) {
				bean.setChecked(true);
				return bean.getPos();
			}
		}
		return null;
	}

	/**
	 * find new neighbor at pos
	 * 
	 * @param world the world
	 * @param pos   the position
	 * @param found the set with all already found positions
	 * @return true or false
	 */
	private static final boolean findNewNeihgbor(World world, BlockPos pos, Set<NeighborhoodBean> found,
			BiFunction<World, BlockPos, Boolean> checkLambda) {
		NeighborhoodBean bean = new NeighborhoodBean(pos);
		if (found.contains(bean) || found.contains(bean.over()) || found.contains(bean.below())) {
			return false;
		} else if (checkLambda.apply(world, pos).booleanValue()) {
			found.add(bean);
			return true;
		} else if (checkLambda.apply(world, pos.up()).booleanValue()) {
			found.add(new NeighborhoodBean(pos.up()));
			return true;
		} else if (checkLambda.apply(world, pos.down()).booleanValue()) {
			found.add(new NeighborhoodBean(pos.down()));
			return true;
		}
		return false;
	}
}
