package de.jottyfan.minecraft.quickiefabric.items;

import java.util.Iterator;
import java.util.Set;

import de.jottyfan.minecraft.quickiefabric.api.Neighborhood;
import de.jottyfan.minecraft.quickiefabric.container.BackpackInventory;
import de.jottyfan.minecraft.quickiefabric.item.ModdedItemUsageContext;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class ItemBag extends ItemBackpack {

	private Integer useItemsOfStack(ItemStack stack, Set<BlockPos> posList, ItemUsageContext context, World world,
			PlayerEntity player, Item item) {
		Iterator<BlockPos> iterator = posList.iterator();
		while (iterator.hasNext() && stack.getCount() > 0) {
			BlockPos pos = iterator.next();
			BlockHitResult hit = new BlockHitResult(context.getHitPos(), context.getSide(), pos, context.hitsInsideBlock());
			ModdedItemUsageContext iuc = new ModdedItemUsageContext(world, player, Hand.MAIN_HAND, stack, hit);
			item.useOnBlock(iuc);
			stack.decrement(1);
			iterator.remove();
		}
		return stack.getCount();
	}

	@Override
	public ActionResult useOnBlock(ItemUsageContext context) {
		BlockPos pos = context.getBlockPos();
		World world = context.getWorld();
		PlayerEntity player = context.getPlayer();
		if (isFreeFarmland(world, pos)) {
			BackpackInventory bi = new BackpackInventory(context.getStack());
			Set<BlockPos> posList = Neighborhood.findEqualBlock(world, pos, 4096, (w, p) -> {
				return Blocks.FARMLAND.equals(w.getBlockState(p).getBlock()) && w.getBlockState(p.up()).isAir();
			});
			for (int slot = 0; slot < ItemBackpack.SLOTSIZE; slot++) {
				ItemStack stack = bi.getStack(slot);
				if (stack.getCount() > 0) {
					for (int i = stack.getCount(); i > 0; i--) {
						int beforeUsage = stack.getCount();
						int afterUsage = useItemsOfStack(stack, posList, context, world, player, stack.getItem());
						if (afterUsage == beforeUsage) {
							break; // no more fields found
						} else {
							stack.setCount(afterUsage);
							bi.setStack(slot, stack);
						}
					}
				}
			}
			ItemStack thisBackpack = context.getStack();
			BackpackInventory.setItemsToBackpack(thisBackpack, bi);
			player.setStackInHand(context.getHand(), thisBackpack);
			return ActionResult.SUCCESS;
		} else {
			return super.useOnBlock(context);
		}
	}

	/**
	 * check if the block on pos is farmland and the one above is air
	 *
	 * @param world the world
	 * @param pos   the position
	 * @return true or false
	 */
	private final boolean isFreeFarmland(World world, BlockPos pos) {
		return Blocks.FARMLAND.equals(world.getBlockState(pos).getBlock()) && world.getBlockState(pos.up()).isAir();
	}
}
