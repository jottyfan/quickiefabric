package de.jottyfan.minecraft.quickiefabric.items;

import de.jottyfan.minecraft.quickiefabric.blocks.QuickieBlocks;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class ItemCottonseed extends Item {

	public ItemCottonseed() {
		super(new FabricItemSettings().maxCount(64));
	}

	@Override
	public ActionResult useOnBlock(ItemUsageContext context) {
		BlockPos pos = context.getBlockPos();
		World world = context.getWorld();
		if (QuickieItems.COTTONSEED.equals(context.getStack().getItem())) {
			BlockState state = world.getBlockState(pos);
			if (Blocks.FARMLAND.equals(state.getBlock()) && world.getBlockState(pos.up()).isAir()) {
				world.setBlockState(pos.up(), QuickieBlocks.COTTONPLANT.getDefaultState());
				context.getStack().decrement(1);
			}
		}
		return super.useOnBlock(context);
	}
}
