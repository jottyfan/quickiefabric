package de.jottyfan.minecraft.quickiefabric.items;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;

/**
 *
 * @author jotty
 *
 */
public class ItemCarrotstack extends Item {

	public ItemCarrotstack() {
		super(new FabricItemSettings().maxCount(64)
				.food(new FoodComponent.Builder().hunger(12).saturationModifier(0.6F).build()));
	}
}
