package de.jottyfan.minecraft.quickiefabric.items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.jottyfan.minecraft.quickiefabric.container.BackpackInventory;
import de.jottyfan.minecraft.quickiefabric.container.BackpackScreenHandler;
import de.jottyfan.minecraft.quickiefabric.init.RegistryManager;
import io.netty.buffer.Unpooled;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.LootableContainerBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class ItemBackpack extends Item {
	public final static Integer SLOTSIZE = 54;

	public ItemBackpack() {
		super(new FabricItemSettings().maxCount(1));
	}

	@Override
	public TypedActionResult<ItemStack> use(World world, PlayerEntity player, Hand hand) {
		if (player.isSneaking()) {
			player.setStackInHand(hand, sortBackpackContent(player.getStackInHand(hand)));
		}
		if (!world.isClient) {
			final ItemStack itemStack = player.getStackInHand(hand);
			player.openHandledScreen(new ExtendedScreenHandlerFactory() {
				@Override
				public void writeScreenOpeningData(ServerPlayerEntity serverPlayerEntity, PacketByteBuf packetByteBuf) {
					packetByteBuf.writeItemStack(itemStack);
					packetByteBuf.writeByte(hand == Hand.MAIN_HAND ? 1 : 0);
				}

				@Override
				public Text getDisplayName() {
					return Text.of(RegistryManager.BACKPACK_IDENTIFIER.toString());
				}

				@Override
				public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
					PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
					buf.writeItemStack(itemStack);
					buf.writeByte(hand == Hand.MAIN_HAND ? 1 : 0);
					return new BackpackScreenHandler(syncId, inv, buf);
				}
			});
		}
		return new TypedActionResult<ItemStack>(ActionResult.SUCCESS, player.getStackInHand(hand));
	}

	/**
	 * sort the content of the backpack; for spreaded items, combine them until the
	 * itemstack size is reached
	 *
	 * @param itemStack the itemStack of the backpack
	 * @return the itemStack of the sorted backpack
	 */
	private ItemStack sortBackpackContent(ItemStack itemStack) {
		if (!itemStack.isEmpty()) {
			List<ItemStack> stacks = BackpackInventory.getItemsFromBackpack(itemStack);
			Map<String, List<ItemStack>> map = new HashMap<>();
			for (ItemStack stack : stacks) {
				String id = stack.getItem().getTranslationKey();
				List<ItemStack> foundStacks = map.get(id);
				if (foundStacks == null) {
					foundStacks = new ArrayList<>();
					map.put(id, foundStacks);
				}
				foundStacks.add(stack);
			}
			BackpackInventory.setItemsToBackpack(itemStack, map.values());
		}
		return itemStack;
	}

	@Override
	public ActionResult useOnBlock(ItemUsageContext context) {
		BlockPos pos = context.getBlockPos();
		World world = context.getWorld();
		PlayerEntity player = context.getPlayer();
		BlockEntity entity = world.getBlockEntity(pos);
		if (entity != null && entity instanceof LootableContainerBlockEntity) {
			BackpackInventory bi = new BackpackInventory(context.getStack());
			bi.setHand(context.getHand());
			LootableContainerBlockEntity blockEntity = (LootableContainerBlockEntity) entity;
			if (bi.isEmpty()) { // fill backpack
				for (int slot = 0; slot < ItemBackpack.SLOTSIZE; slot++) {
					if (slot < blockEntity.size()) {
						bi.setStack(slot, blockEntity.getStack(slot));
						blockEntity.setStack(slot, ItemStack.EMPTY);
					}
				}
				bi.onClose(player);
				if (!world.isClient) {
					player.sendMessage(Text.translatable("msg.backpack.transfer.filled"), false);
				}
			} else { // empty backpack as long as possible
				for (int slot = 0; slot < ItemBackpack.SLOTSIZE; slot++) {
					List<Integer> slots = findSlots(blockEntity, bi.getStack(slot));
					for (Integer barrelSlot : slots) {
						ItemStack barrelStack = blockEntity.getStack(barrelSlot);
						ItemStack backpackStack = bi.getStack(slot);
						int foundCount = barrelStack.getCount();
						int transferCount = 64 - foundCount;
						int newBackpackCount = backpackStack.getCount() - transferCount;
						if (newBackpackCount < 0) {
							transferCount = backpackStack.getCount();
							newBackpackCount = 0;
						}
						// if it was air, replace it with something
						blockEntity.setStack(barrelSlot, new ItemStack(backpackStack.getItem(), foundCount + transferCount));
						backpackStack.decrement(transferCount);
					}
				}
				bi.onClose(player);
				if (!world.isClient) {
					player.sendMessage(Text.translatable("msg.backpack.transfer.cleared"), false);
				}
			}
		}
		return super.useOnBlock(context);
	}

	/**
	 * find the numbers of the slots that contain items of stack or have Air
	 *
	 * @param blockEntity the block entity
	 * @param stack       the item stack
	 * @return a list of found stack positions; an empty list at least
	 */
	private final List<Integer> findSlots(LootableContainerBlockEntity blockEntity, ItemStack stack) {
		List<Integer> list = new ArrayList<>();
		if (!stack.isEmpty())
			for (int i = 0; i < blockEntity.size(); i++) {
				ItemStack containerStack = blockEntity.getStack(i);
				if (containerStack.isEmpty() || containerStack.getItem().equals(stack.getItem())) {
					list.add(i);
				}
			}
		return list;
	}
}
