package de.jottyfan.minecraft.quickiefabric.items;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.Item;

/**
 *
 * @author jotty
 *
 */
public class ItemLevelup extends Item {

	public ItemLevelup() {
		super(new FabricItemSettings().maxCount(64));
	}
}
