package de.jottyfan.minecraft.quickiefabric.items;

/**
 *
 * @author jotty
 *
 */
public class QuickieItems {
	public static final ItemSpeedpowder SPEEDPOWDER = new ItemSpeedpowder();
	public static final ItemLevelup LEVELUP = new ItemLevelup();
	public static final ItemPencil PENCIL = new ItemPencil();
	public static final ItemSalpeter SALPETER = new ItemSalpeter();
	public static final ItemSulphor SULPHOR = new ItemSulphor();
	public static final ItemBackpack BACKPACK_BROWN = new ItemBackpack();
	public static final ItemBackpack BACKPACK_WHITE = new ItemBackpack();
	public static final ItemBackpack BACKPACK_BLACK = new ItemBackpack();
	public static final ItemBackpack BACKPACK_BLUE = new ItemBackpack();
	public static final ItemBackpack BACKPACK_CYAN = new ItemBackpack();
	public static final ItemBackpack BACKPACK_GREEN = new ItemBackpack();
	public static final ItemBackpack BACKPACK_PINK = new ItemBackpack();
	public static final ItemBackpack BACKPACK_RED = new ItemBackpack();
	public static final ItemBackpack BACKPACK_YELLOW = new ItemBackpack();
	public static final ItemBackpack BACKPACK_DARKGRAY = new ItemBackpack();
	public static final ItemBackpack BACKPACK_LIGHTGRAY = new ItemBackpack();
	public static final ItemBackpack BACKPACK_LIGHTGREEN = new ItemBackpack();
	public static final ItemBackpack BACKPACK_MAGENTA = new ItemBackpack();
	public static final ItemBackpack BACKPACK_ORANGE = new ItemBackpack();
	public static final ItemBackpack BACKPACK_PURPLE = new ItemBackpack();
	public static final ItemBackpack BACKPACK_LIGHTBLUE = new ItemBackpack();
	public static final ItemBag BAG = new ItemBag();
	public static final ItemRottenFleshStripes ROTTEN_FLESH_STRIPES = new ItemRottenFleshStripes();
	public static final ItemCarrotstack CARROTSTACK = new ItemCarrotstack();
	public static final ItemCotton COTTON = new ItemCotton();
	public static final ItemCottonseed COTTONSEED = new ItemCottonseed();
	public static final ItemStub STUB = new ItemStub();
}
