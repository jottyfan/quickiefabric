package de.jottyfan.minecraft.quickiefabric;

import de.jottyfan.minecraft.quickiefabric.init.RegistryManager;
import net.fabricmc.api.ModInitializer;

/**
 *
 * @author jotty
 *
 */
public class QuickieFabric implements ModInitializer {

	@Override
	public void onInitialize() {
		RegistryManager.registerItems();
		RegistryManager.registerTools();
		RegistryManager.registerEvents();
		RegistryManager.registerBlocks();
		RegistryManager.registerBlockEntities();
		RegistryManager.registerFeatures();
		RegistryManager.registerContainer();
		RegistryManager.registerLootings();
		RegistryManager.registerTags();
		RegistryManager.registerItemGroup();
	}
}
