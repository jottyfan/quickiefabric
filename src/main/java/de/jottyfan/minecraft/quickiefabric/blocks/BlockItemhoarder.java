package de.jottyfan.minecraft.quickiefabric.blocks;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.jottyfan.minecraft.quickiefabric.blockentity.ItemHoarderBlockEntity;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContextParameterSet.Builder;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class BlockItemhoarder extends Block implements BlockEntityProvider {

	public BlockItemhoarder() {
		super(FabricBlockSettings.create().hardness(2.5f));
	}

	@Override
	public BlockEntity createBlockEntity(BlockPos pos, BlockState blockState) {
		return new ItemHoarderBlockEntity(pos, blockState);
	}

	@Override
	public BlockRenderType getRenderType(BlockState state) {
		return BlockRenderType.MODEL;
	}

	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type){
		return (world1, pos, state1, be) -> ItemHoarderBlockEntity.tick(world1, pos, state1, be);
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState state, Builder builder) {
		List<ItemStack> list = new ArrayList<>();
		list.add(new ItemStack(QuickieBlocks.ITEMHOARDER));
		return list;
	}

	@Override
	public void onBreak(World world, BlockPos pos, BlockState state, PlayerEntity player) {
		BlockEntity blockEntity = world.getBlockEntity(pos);
		if (blockEntity instanceof ItemHoarderBlockEntity) {
			ItemHoarderBlockEntity ihbe = (ItemHoarderBlockEntity) blockEntity;
			for (ItemStack stack : ihbe.getStacks()) {
				ItemScatterer.spawn(world, pos.getX(), pos.getY(), pos.getZ(), stack);
			}
		}
		super.onBreak(world, pos, state, player);
	}

	@Override
	public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
			BlockHitResult hit) {
		if (!world.isClient) {
			BlockEntity blockEntity = world.getBlockEntity(pos);
			if (blockEntity instanceof ItemHoarderBlockEntity) {
				ItemHoarderBlockEntity ihbe = (ItemHoarderBlockEntity) blockEntity;
				player.sendMessage(Text.translatable("msg.itemhoarder.summary",
						new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(new Date())).setStyle(Style.EMPTY.withColor(Formatting.BOLD)), false);
				for (Text text : ihbe.getSummary()) {
					player.sendMessage(text, false);
				}
			}
		}
		return ActionResult.SUCCESS;
	}
}
