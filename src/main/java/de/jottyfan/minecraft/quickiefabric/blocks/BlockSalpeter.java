package de.jottyfan.minecraft.quickiefabric.blocks;

import java.util.Arrays;
import java.util.List;

import de.jottyfan.minecraft.quickiefabric.items.QuickieItems;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.BlockState;
import net.minecraft.block.ExperienceDroppingBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContextParameterSet.Builder;

/**
 *
 * @author jotty
 *
 */
public class BlockSalpeter extends ExperienceDroppingBlock {

	public BlockSalpeter() {
		super(FabricBlockSettings.create().hardness(0.5f));
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState state, Builder builder) {
		return Arrays.asList(new ItemStack[] { new ItemStack(QuickieItems.SALPETER, 9) });
	}
}
