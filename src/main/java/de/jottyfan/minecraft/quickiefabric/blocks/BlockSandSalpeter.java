package de.jottyfan.minecraft.quickiefabric.blocks;

import java.util.Arrays;
import java.util.List;

import de.jottyfan.minecraft.quickiefabric.items.QuickieItems;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.GravelBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContextParameterSet.Builder;
import net.minecraft.util.math.random.Random;

/**
 *
 * @author jotty
 *
 */
public class BlockSandSalpeter extends GravelBlock {

	public BlockSandSalpeter() {
		super(FabricBlockSettings.create().hardness(3.1f).requiresTool());
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState state, Builder builder) {
			return Arrays.asList(new ItemStack[] { new ItemStack(QuickieItems.SALPETER, 3 + Random.create().nextInt(2)), new ItemStack(Blocks.SAND) });
	}
}
