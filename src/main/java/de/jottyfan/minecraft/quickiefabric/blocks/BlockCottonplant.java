package de.jottyfan.minecraft.quickiefabric.blocks;

import java.util.List;
import java.util.Random;

import de.jottyfan.minecraft.quickiefabric.items.QuickieItems;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CropBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContextParameterSet.Builder;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockCottonplant extends CropBlock {

	public BlockCottonplant() {
		super(FabricBlockSettings.copy(Blocks.WHEAT));
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState state, Builder builder) {
		DefaultedList<ItemStack> list = DefaultedList.of();
		list.add(new ItemStack(getSeedsItem())); // the one from the seed
		if (isMature(state)) {
			list.add(new ItemStack(getSeedsItem(), new Random().nextInt(2)));
			list.add(new ItemStack(QuickieItems.COTTON, new Random().nextFloat() > 0.9f ? 2 : 1));
		}
		return list;
	}

	private void spawnHarvested(World world, BlockPos pos, BlockState state) {
		DefaultedList<ItemStack> list = DefaultedList.of();
		getDroppedStacks(state, null).forEach(itemStack -> {
			list.add(itemStack);
		});
		ItemScatterer.spawn(world, pos, list);
	}

	@Override
	public void onBreak(World world, BlockPos pos, BlockState state, PlayerEntity player) {
		spawnHarvested(world, pos, state);
		super.onBreak(world, pos, state, player);
	}

	@Override
	protected ItemConvertible getSeedsItem() {
		return QuickieItems.COTTONSEED;
	}

	@Override
	public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
			BlockHitResult hitResult) {
		if (!world.isClient && isMature(state)) {
			spawnHarvested(world, pos, state);
			world.setBlockState(pos, state.with(AGE, 0));
		}
		return super.onUse(state, world, pos, player, hand, hitResult);
	}
}