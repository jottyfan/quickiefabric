package de.jottyfan.minecraft.quickiefabric.blocks;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import de.jottyfan.minecraft.quickiefabric.items.QuickieItems;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.GravelBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.context.LootContextParameterSet.Builder;

/**
 *
 * @author jotty
 *
 */
public class BlockDirtSalpeter extends GravelBlock {

	public BlockDirtSalpeter() {
		super(FabricBlockSettings.create().hardness(3.1f));
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState blockState, Builder builder) {
		Integer count = (Double.valueOf(new Random().nextDouble() * 10).intValue() / 4) + 1;
		boolean spawnBoneMeal = new Random().nextDouble() > 0.5f;
		ItemStack boneMealStack = new ItemStack(Items.BONE_MEAL);
		ItemStack salpeterStack = new ItemStack(QuickieItems.SALPETER, count);
		ItemStack dirtStack = new ItemStack(Blocks.DIRT);
		ItemStack[] spawnies = spawnBoneMeal ? new ItemStack[] { boneMealStack, salpeterStack, dirtStack }
				: new ItemStack[] { salpeterStack, dirtStack };
		return Arrays.asList(spawnies);
	}
}
