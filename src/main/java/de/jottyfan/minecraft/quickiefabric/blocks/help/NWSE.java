package de.jottyfan.minecraft.quickiefabric.blocks.help;

/**
 * 
 * @author jotty
 *
 */
public enum NWSE {
	NORTH, SOUTH, EAST, WEST, NONE;
}
