package de.jottyfan.minecraft.quickiefabric.blocks;

import java.util.ArrayList;
import java.util.List;

import de.jottyfan.minecraft.quickiefabric.blockentity.DrillBlockNorthEntity;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.FallingBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContextParameterSet.Builder;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public class BlockDrillNorth extends FallingBlock implements BlockEntityProvider {
	
	public BlockDrillNorth() {
		super(FabricBlockSettings.create().hardness(2.5f));
	}

	@Override
	public BlockEntity createBlockEntity(BlockPos pos, BlockState blockState) {
		return new DrillBlockNorthEntity(pos, blockState);
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState state, Builder builder) {
		List<ItemStack> list = new ArrayList<>();
		list.add(new ItemStack(QuickieBlocks.DRILL_NORTH));
		return list;
	}

	@Override
	public BlockRenderType getRenderType(BlockState state) {
		return BlockRenderType.MODEL;
	}

	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type){
		return (world1, pos, state1, be) -> DrillBlockNorthEntity.tick(world1, pos, state1, be);
	}
}
