package de.jottyfan.minecraft.quickiefabric.blocks;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import de.jottyfan.minecraft.quickiefabric.blockentity.EmptyLavaHoarderBlockEntity;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContextParameterSet.Builder;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class BlockEmptyLavahoarder extends Block implements BlockEntityProvider {

	public BlockEmptyLavahoarder() {
		super(FabricBlockSettings.create().hardness(2.5f));
	}

	@Override
	public BlockEntity createBlockEntity(BlockPos pos, BlockState blockState) {
		return new EmptyLavaHoarderBlockEntity(pos, blockState);
	}

	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type){
		return (world1, pos, state1, be) -> EmptyLavaHoarderBlockEntity.tick(world1, pos, state1, be);
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState state, Builder builder) {
		List<ItemStack> list = new ArrayList<>();
		list.add(new ItemStack(QuickieBlocks.EMPTYLAVAHOARDER));
		return list;
	}

	private static final String stringOf(BlockPos pos) {
		StringBuilder buf = new StringBuilder();
		buf.append(pos.getX()).append(":");
		buf.append(pos.getY()).append(":");
		buf.append(pos.getZ());
		return buf.toString();
	}

	private static final BlockPos blockPosOf(String s) {
		if (s.contains(":")) {
			String[] parts = s.split(":");
			if (parts.length > 2) {
				Integer x = Integer.valueOf(parts[0]);
				Integer y = Integer.valueOf(parts[1]);
				Integer z = Integer.valueOf(parts[2]);
				return new BlockPos(x, y, z);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private void findAllAttachedLavaBlocks(Set<String> list, BlockPos pos, World world, Integer counter) {
		if (counter < 1) {
			return;
		} else if (Blocks.LAVA.equals(world.getBlockState(pos).getBlock())) {
			String p = stringOf(pos);
			if (!list.contains(p)) {
				list.add(p);
				findAllAttachedLavaBlocks(list, pos.up(), world, counter - 1);
				findAllAttachedLavaBlocks(list, pos.down(), world, counter - 1);
				findAllAttachedLavaBlocks(list, pos.north(), world, counter - 1);
				findAllAttachedLavaBlocks(list, pos.south(), world, counter - 1);
				findAllAttachedLavaBlocks(list, pos.east(), world, counter - 1);
				findAllAttachedLavaBlocks(list, pos.west(), world, counter - 1);
			}
		}
	}

	@Override
	public void onPlaced(World world, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack) {
		Set<String> positions = new HashSet<>();
		Integer counter = 8; // TODO: make it level up - able
		findAllAttachedLavaBlocks(positions, pos.up(), world, counter);
		findAllAttachedLavaBlocks(positions, pos.down(), world, counter);
		findAllAttachedLavaBlocks(positions, pos.north(), world, counter);
		findAllAttachedLavaBlocks(positions, pos.south(), world, counter);
		findAllAttachedLavaBlocks(positions, pos.east(), world, counter);
		findAllAttachedLavaBlocks(positions, pos.west(), world, counter);
		Integer amount = positions.size();
		for (String p : positions) {
			world.setBlockState(blockPosOf(p), Blocks.AIR.getDefaultState());
		}
		if (amount > 0) {
			world.setBlockState(pos, QuickieBlocks.LAVAHOARDER.getDefaultState());
			int count = 0;
			Random random = new Random();
			for (int i = 0; i < amount; i++) {
				if (random.nextFloat() < 0.0125) {
					count++;
				}
			}
			BlockPos up = pos.up();
			if (count > 0) {
				EmptyLavaHoarderBlockEntity.spawnRandomItems(world, up, count);
				world.spawnEntity(new ExperienceOrbEntity(world, (double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D, (double) pos.getZ() + 0.5D, count));
			}
		}
	}
}
