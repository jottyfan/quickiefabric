package de.jottyfan.minecraft.quickiefabric.blocks;

import java.util.Arrays;
import java.util.List;

import de.jottyfan.minecraft.quickiefabric.items.QuickieItems;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.BlockState;
import net.minecraft.block.ExperienceDroppingBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContextParameterSet.Builder;


/**
 *
 * @author jotty
 *
 */
public class BlockOreDeepslateSulphor extends ExperienceDroppingBlock {

	public BlockOreDeepslateSulphor() {
		super(FabricBlockSettings.create().hardness(1.9f).requiresTool());
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState state, Builder builder) {
		return Arrays.asList(new ItemStack[] { new ItemStack(QuickieItems.SULPHOR, 4) });
	}
}
