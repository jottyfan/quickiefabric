package de.jottyfan.minecraft.quickiefabric.blocks;

import java.util.Arrays;
import java.util.List;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.BlockState;
import net.minecraft.block.GravelBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.context.LootContextParameterSet.Builder;
import net.minecraft.sound.BlockSoundGroup;

/**
 *
 * @author jotty
 *
 */
public class BlockKelpstack extends GravelBlock {

	public BlockKelpstack() {
		super(FabricBlockSettings.create().hardness(0.1f).slipperiness(1.0f)
				.breakInstantly().sounds(BlockSoundGroup.WET_GRASS));
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState blockState, Builder builder) {
		return Arrays.asList(new ItemStack[] { new ItemStack(Items.KELP, 9) });
	}
}
