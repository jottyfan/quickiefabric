package de.jottyfan.minecraft.quickiefabric.blocks;

import java.util.ArrayList;
import java.util.List;

import de.jottyfan.minecraft.quickiefabric.blockentity.EmptyLavaHoarderBlockEntity;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.context.LootContextParameterSet.Builder;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class BlockLavahoarder extends Block implements BlockEntityProvider {

	public BlockLavahoarder() {
		super(FabricBlockSettings.create().hardness(2.5f).luminance(16));
	}

	@Override
	public BlockEntity createBlockEntity(BlockPos pos, BlockState blockState) {
		return new EmptyLavaHoarderBlockEntity(pos, blockState);
	}

	@Override
	public BlockRenderType getRenderType(BlockState state) {
		return BlockRenderType.MODEL;
	}

	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type){
		return (world1, pos, state1, be) -> EmptyLavaHoarderBlockEntity.tick(world1, pos, state1, be);
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState state, Builder builder) {
		List<ItemStack> list = new ArrayList<>();
		list.add(new ItemStack(QuickieBlocks.LAVAHOARDER));
		return list;
	}

	@Override
	public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand,
			BlockHitResult hit) {
		if (!world.isClient) {
			ItemStack handStack = player.getStackInHand(hand);
			if (handStack != null && Items.BUCKET.equals(handStack.getItem())) {
				Integer amount = handStack.getCount();
				ItemStack lavaBucketStack = new ItemStack(Items.LAVA_BUCKET, 1);
				ItemStack emptyBucketStack = new ItemStack(Items.BUCKET, amount - 1);
				if (emptyBucketStack.getCount() < 1) {
					player.setStackInHand(hand, lavaBucketStack);
				} else {
					player.setStackInHand(hand, emptyBucketStack);
					world.spawnEntity(new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), lavaBucketStack));
				}
				EmptyLavaHoarderBlockEntity.spawnRandomItems(world, pos, 2);
				world.setBlockState(pos, QuickieBlocks.EMPTYLAVAHOARDER.getDefaultState());
			}
		}
		return ActionResult.SUCCESS; // forbid to empty the just filled lava bucket
	}
}
