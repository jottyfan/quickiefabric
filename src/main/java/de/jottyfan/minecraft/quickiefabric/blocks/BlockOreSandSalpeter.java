package de.jottyfan.minecraft.quickiefabric.blocks;

import java.util.Arrays;
import java.util.List;

import de.jottyfan.minecraft.quickiefabric.items.QuickieItems;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.ExperienceDroppingBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContextParameterSet.Builder;
import net.minecraft.util.math.random.Random;

/**
 *
 * @author jotty
 *
 */
public class BlockOreSandSalpeter extends ExperienceDroppingBlock {

	public BlockOreSandSalpeter() {
		super(FabricBlockSettings.create().hardness(2.9f).requiresTool());
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState state, Builder builder) {
		return Arrays.asList(new ItemStack[] { new ItemStack(QuickieItems.SALPETER, 7 + Random.create().nextInt(4)), new ItemStack(Blocks.SAND) });
	}
}
