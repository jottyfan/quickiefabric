package de.jottyfan.minecraft.quickiefabric.blocks;

import java.util.List;

import de.jottyfan.minecraft.quickiefabric.blockentity.BlockSpreaderEntity;
import de.jottyfan.minecraft.quickiefabric.blockentity.QuickieFabricBlockEntity;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContextParameterSet.Builder;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 *
 * @author jotty
 *
 */
public class BlockSpreader extends BlockWithEntity {

	public BlockSpreader() {
		super(FabricBlockSettings.create().hardness(2.5f));
	}

	@Override
	public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
		return new BlockSpreaderEntity(pos, state);
	}

	@Override
	public BlockRenderType getRenderType(BlockState state) {
		return BlockRenderType.MODEL;
	}

	@Override
	public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state,
			BlockEntityType<T> type) {
		return validateTicker(type, QuickieFabricBlockEntity.BLOCKSPREADER_ENTITY,
				(world1, pos, state1, be) -> BlockSpreaderEntity.tick(world1, pos, state1, be));
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState state, Builder builder) {
		return List.of(new ItemStack(QuickieBlocks.BLOCKSPREADER.asItem()));
	}
}
