package de.jottyfan.minecraft.quickiefabric.blocks;

/**
 *
 * @author jotty
 *
 */
public class QuickieBlocks {
	public static final BlockDirtSalpeter DIRT_SALPETER = new BlockDirtSalpeter();
	public static final BlockOreNetherSulphor ORE_NETHER_SULPHOR = new BlockOreNetherSulphor();
	public static final BlockOreSalpeter ORE_SALPETER = new BlockOreSalpeter();
	public static final BlockOreSandSalpeter ORE_SAND_SALPETER = new BlockOreSandSalpeter();
	public static final BlockOreSulphor ORE_SULPHOR = new BlockOreSulphor();
	public static final BlockOreDeepslateSulphor ORE_DEEPSLATESULPHOR = new BlockOreDeepslateSulphor();
	public static final BlockSandSalpeter SAND_SALPETER = new BlockSandSalpeter();
	public static final BlockLavahoarder LAVAHOARDER = new BlockLavahoarder();
	public static final BlockEmptyLavahoarder EMPTYLAVAHOARDER = new BlockEmptyLavahoarder();
	public static final BlockItemhoarder ITEMHOARDER = new BlockItemhoarder();
	public static final BlockMonsterhoarder MONSTERHOARDER = new BlockMonsterhoarder();
	public static final BlockKelpstack KELPSTACK = new BlockKelpstack();
	public static final BlockCottonplant COTTONPLANT = new BlockCottonplant();
	public static final BlockSulphor BLOCKSULPHOR = new BlockSulphor();
	public static final BlockSalpeter BLOCKSALPETER = new BlockSalpeter();
	public static final BlockDrillDown DRILL_DOWN = new BlockDrillDown();
	public static final BlockDrillEast DRILL_EAST = new BlockDrillEast();
	public static final BlockDrillSouth DRILL_SOUTH = new BlockDrillSouth();
	public static final BlockDrillWest DRILL_WEST = new BlockDrillWest();
	public static final BlockDrillNorth DRILL_NORTH = new BlockDrillNorth();
	public static final BlockDrillstop DRILLSTOP = new BlockDrillstop();
	public static final BlockStackerUp BLOCKSTACKERUP = new BlockStackerUp();
	public static final BlockStackerDown BLOCKSTACKERDOWN = new BlockStackerDown();
	public static final BlockStackerEast BLOCKSTACKEREAST = new BlockStackerEast();
	public static final BlockStackerWest BLOCKSTACKERWEST = new BlockStackerWest();
	public static final BlockStackerNorth BLOCKSTACKERNORTH = new BlockStackerNorth();
	public static final BlockStackerSouth BLOCKSTACKERSOUTH = new BlockStackerSouth();
	public static final BlockSpreader BLOCKSPREADER = new BlockSpreader();
}
