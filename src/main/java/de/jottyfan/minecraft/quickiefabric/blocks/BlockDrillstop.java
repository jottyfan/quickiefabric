package de.jottyfan.minecraft.quickiefabric.blocks;

import java.util.ArrayList;
import java.util.List;

import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContextParameterSet.Builder;


/**
 * 
 * @author jotty
 *
 */
public class BlockDrillstop extends Block {

	public BlockDrillstop() {
		super(FabricBlockSettings.create().hardness(2.5f));
	}

	@Override
	public List<ItemStack> getDroppedStacks(BlockState state, Builder builder) {
		List<ItemStack> list = new ArrayList<>();
		list.add(new ItemStack(QuickieBlocks.DRILLSTOP));
		return list;
	}
}
